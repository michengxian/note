# SpringBoot

## 常见

### SpringBoot提供了哪些核心功能

- 独立运行Spring项目
  - 可以打包成jar包，运行时java -jar xx.jar运行jar包
- 内嵌Servlet容器
  - 可以内嵌Tomcat、Jetty，无需以War包形式部署
- 提供Starter简化Maven配置
  - 如spring-boot-starter-web
- 自动配置Spring Bean
- 应用监控
- 无代码生成和XMl配置

### SpringBoot优缺点

- 优点：
  - 编码简单
  - 配置简单
  - 部署简单
  - 监控简单
- 缺点：
  - 没有服务发现和注册
  - 没有配套的安全管控方案
  - 
