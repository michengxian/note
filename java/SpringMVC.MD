# SpringMVC


## 搭建环境

创建一个maven项目

### pom.xml

```xml
<dependencies>

    <!--spring核心包-->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-core</artifactId>
      <version>5.2.10.RELEASE</version>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-web</artifactId>
      <version>5.2.10.RELEASE</version>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-oxm</artifactId>
      <version>5.2.10.RELEASE</version>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-tx</artifactId>
      <version>5.2.10.RELEASE</version>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-webmvc</artifactId>
      <version>5.2.10.RELEASE</version>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-aop</artifactId>
      <version>5.2.10.RELEASE</version>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-context-support</artifactId>
      <version>5.2.10.RELEASE</version>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-test</artifactId>
      <version>5.2.10.RELEASE</version>
    </dependency>


    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <version>1.18.20</version>
    </dependency>

    <dependency>
      <groupId>javax.servlet</groupId>
      <artifactId>javax.servlet-api</artifactId>
      <version>4.0.1</version>
    </dependency>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.11</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
```

### web.xml

```xml
<!DOCTYPE web-app PUBLIC
 "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN"
 "http://java.sun.com/dtd/web-app_2_3.dtd" >

<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
         version="3.1">
  <display-name>Archetype Created Web Application</display-name>

  <welcome-file-list>
    <welcome-file>index.jsp</welcome-file>
  </welcome-file-list>
  
  <servlet>
    <servlet-name>springMVC</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
    <init-param>
      <param-name>contextConfigLocation</param-name>
      <param-value>WEB-INF/dispatcher-servlet.xml</param-value>
    </init-param>
    <load-on-startup>1</load-on-startup>
  </servlet>
  
  <servlet-mapping>
    <servlet-name>springMVC</servlet-name>
    <url-pattern>/</url-pattern>
  </servlet-mapping>

<!--  <context-param>-->
<!--    <param-name>contextConfigLocation</param-name>-->
<!--    <param-value>WEB-INF/applicationContext.xml</param-value>-->
<!--  </context-param>-->
  
  <listener>
    <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
  </listener>
  
</web-app>
```

### dispatcher-servlet.xml

新建 dispatcher-servlet.xml 文件

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/mvc http://www.springframework.org/schema/mvc/spring-mvc.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

<!--    spring的一些annotation-->
    <context:annotation-config/>

<!--    配置注解驱动，将request参数绑定到controller参数上-->
    <mvc:annotation-driven/>


<!--    自动扫码装配-->
    <context:component-scan base-package="com.mi.learn.springmvc"/>

</beans>
```


### applicationContext.xml

根据web.xml配置，可新建applicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">

    <context:component-scan base-package="com.mi.learn.springmvc"/>

</beans>
```

### repository

```java
@Repository
public class TestDemoRepository {

    public String testDemoMethod(){
        return "TestDemoRepository.testDemoMethod success";
    }

}
```

### serer

```java
@Service
public class TestDemoService {

    @Autowired
    private TestDemoRepository testDemoRepository;

    public String testDemoMethod(){
        return "TestDemoService.testDemoMethod :"+testDemoRepository.testDemoMethod();
    }

}
```

### controller

```java
@Controller
public class TestDemoController {

    @Autowired
    private TestDemoService service;

    @RequestMapping(value = "/test" ,method = RequestMethod.GET)
    @ResponseBody
    public String test(){
        return "TestDemoController." + service.testDemoMethod();
    }
}
```


## 注解

### @RestController、@Controller

@Controller：标记为Spring Web MVC控制器Controller

@ResponseBody：根据客户端的ACCEPT请求头决定返回的数据格式

@RestController = @Controller + @ResponseBody

### @RequestMapping、@GetMapping

@RequestMapping可注解在类和接口上，映射请求的URL，在方法上可声明请求方式，GET、HEAD、POST、PUT、PATCH、DELETE、OPTIONS、TRACE

@GetMapping只可注解在接口上，只支持GET请求

### Spring MVC异常处理

方式一：@ControllerAdvice注解

全局的Exception都会到exceptionHandler方法中

```java
@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Object exceptionHandler(Exception e){
        System.out.println("CustomExceptionHandler.exceptionHandler");
        e.printStackTrace();
        return null;
    }

}
```

方式二：使用的Controller继承BaseController

```java
public class BaseController {

    @ExceptionHandler
    @ResponseBody
    public Object exceptionHandler(Exception e){
        System.out.println("BaseController.exceptionHandler");
        e.printStackTrace();
        return "BaseController.exceptionHandler";
    }
}
```

方式三：使用的Controller实现DataExceptionResolver接口

```java
public interface DataExceptionResolver {

    @ExceptionHandler
    @ResponseBody
    default Object exceptionHandler(Exception e){
        try {
            throw e;
        }
        catch (Exception exception){
            System.out.println("DataExceptionResolver.exceptionHandler");
            exception.printStackTrace();
        }
        return "DataExceptionResolver.exceptionHandler";
    }

}
```

方式四：实现HandlerExceptionResolver接口，返回自定义JSON字符串

```java
@Component
public class MyHandler implements HandlerExceptionResolver {
    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        String msg = "系统错误";
        String code = "10001";
        String detail = "请联系管理员";
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("msg", msg);
            map.put("code", code);
            map.put("detail", detail);
            httpServletResponse.setContentType("text/html;charset=utf-8");
            httpServletResponse.getWriter().print(JSON.toJSON(map));
        }catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }
}
```

返回

```json
{"msg":"系统错误","code":"10001","detail":"请联系管理员"}
```

方式四：实现HandlerExceptionResolver接口，返回ModelAndView

```java
@Component
public class MyHandler implements HandlerExceptionResolver {
    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("WEB-INF/error.jsp");
        mv.addObject("exception", e.toString().replaceAll("\n", "<br/>"));
        return mv;
    }
}
```

error.jsp

```jsp
<html>
<body>
<h2>${exception}</h2>
</body>
</html>
```

返回：

```json
java.lang.RuntimeException: except
```


### Struts2和SpringMVC

- Struts2
  - 类级别的拦截
  - 一个类对应一个request
  - 一个Action的一个方法对应一个url，类的属性，所有方法共享
  - 每次请求来创建一个Action，一个Action对象对应一个request上下文
- SpringMVC
  - 方法级别的拦截
  - 一个方法对应一个request
  - 一个方法对应一个url
  - 方法之间不共享变量
  - request获取参数
  - response返回参数
  - 处理结果通过ModelAndView交给架构


### 过滤器

方式一：实现Filter接口

```java
public class CustomFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("CustomFilter.init");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("CustomFilter.doFilter start");
        filterChain.doFilter(servletRequest, servletResponse);
        System.out.println("CustomFilter.doFilter end");
    }
}
```

web.xml

```xml
<context-param>
    <param-name>contextConfigLocation</param-name>
    <param-value>
      WEB-INF/applicationContext.xml,
      WEB-INF/fitter-beans.xml
    </param-value>
  </context-param>

<filter>
    <filter-name>customFilter</filter-name>
    <filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
    <init-param>
      <param-name>targetFilterLifecycle</param-name>
      <param-value>true</param-value>
    </init-param>
  </filter>
  <filter-mapping>
    <filter-name>customFilter</filter-name>
    <url-pattern>/*</url-pattern>
  </filter-mapping>
```

fitter-beans.xml

```xml
<bean id="customFilter" class="com.mi.learn.springmvc.filter.CustomFilter"/>
```

方式二：实现OncePerRequestFilter接口

```java
public class CustomRequestFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        System.out.println("CustomFilter.doFilter start");
        filterChain.doFilter(httpServletRequest, httpServletResponse);
        System.out.println("CustomFilter.doFilter end");
    }
}
```

web.xml

```xml
<filter>
    <filter-name>customRequestFilter</filter-name>
    <filter-class>com.mi.learn.springmvc.filter.CustomRequestFilter</filter-class>
  </filter>
  <filter-mapping>
    <filter-name>customRequestFilter</filter-name>
    <url-pattern>/*</url-pattern>
  </filter-mapping>
```

### 拦截器

```java
public class CustomHandlerInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("CustomHandlerInterceptor.preHandle");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
        System.out.println("CustomHandlerInterceptor.postHandle");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
        System.out.println("CustomHandlerInterceptor.afterCompletion");
    }
}
```

web.xml

```xml
<context-param>
    <param-name>contextConfigLocation</param-name>
    <param-value>
      classpath:beans.xml
    </param-value>
  </context-param>

  <servlet>
    <servlet-name>springMVC</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
    <init-param>
      <param-name>contextConfigLocation</param-name>
      <param-value></param-value>
    </init-param>
    <load-on-startup>1</load-on-startup>
  </servlet>

  <servlet-mapping>
    <servlet-name>springMVC</servlet-name>
    <url-pattern>/</url-pattern>
  </servlet-mapping>

  <listener>
    <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
  </listener>
```

beans.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans-4.3.xsd
        http://www.springframework.org/schema/mvc
        http://www.springframework.org/schema/mvc/spring-mvc-4.3.xsd
        http://www.springframework.org/schema/context
        http://www.springframework.org/schema/context/spring-context-4.3.xsd">

    <mvc:annotation-driven />
    <mvc:default-servlet-handler />

    <!--定义组件扫描器，指定需要扫描的包-->
    <context:component-scan base-package="com.mi.learn.springmvc"/>

    <mvc:interceptors>
        <bean name="customHandlerInterceptor" class="com.mi.learn.springmvc.handler.CustomHandlerInterceptor" />
    </mvc:interceptors>
</beans>
```

DemoController

```java
@Controller
public class DemoController {

    @RequestMapping(value = "/demo",method = RequestMethod.GET)
    @ResponseBody
    public String demo(){
        String res = "DemoController.demo success";
        System.out.println(res);
        return res;
    }
}
```

结果：

```text
CustomHandlerInterceptor.preHandle
DemoController.demo success
CustomHandlerInterceptor.postHandle
CustomHandlerInterceptor.afterCompletion
```

### 拦截器和过滤器

- 功能相同
- 容器不一样
  - 拦截器在SpringMVC体系中
  - Filter在Servlet容器上
- 使用便利性不通
  - 拦截器提供了三个方法，在不同的时机执行
  - 过滤器只提供了一个方法


