# Mysql

## ACID
### A：原子性Automicity

**要么全部成功要么全部失败**


### C：一致性Consistency

**事务前后数据都是完整**

### I：隔离性Isolation
 
**事务之间的数据隔离情况**

- 读未提交 read uncommit 
  - A事务能读取B事务未提交的内容
  - 会导致脏读、不可重复读、幻读
  
- 读提交 read commit
  - A事务只能读取B事务提交后的内容
  - 解决了脏读，会导致不可重复读，幻读
- 可重复读 repeatable read
  - A事务开始后，读到的数据保持不变
  - 解决了脏读、不可重复读，会导致幻读
  - 幻读：A事务查询id=1为空，B插入id=1的数据，A插入id=1会报错，但A查询id=1又为空
    - MVCC：ＭultiVersion Concurrency Control 
      - 只基于InnoDB引擎，基于版本控制，避免同一数据在不同事务间的竞争，有三个隐藏字段（DB_TRX_ID、DB_ROLL_PTR、DB_ROW_ID、FLAG）、undo log、ReadView
    - LBCC
      - 基于锁的并发控制。解决当前读情况下的幻读
        - 锁：
          - 模式：
            - 共享锁S：读锁，当一个事务为数据加读锁后，其他事务不能加写锁，必须所有读锁释放后才能加写锁，读数据的时候不允许修改，避免了重复读的问题
            - 排它锁X：写锁，当一个事务为数据加写锁后，其他请求不能为数据加任何锁，直到该锁释放后，其他事务才能对其加锁，在修改数据的时候，不允许其他人同时修改，也不允许读取，避免出现脏读和脏数据的问题
            - 意向锁：InnoDB所用的表级锁，InnoDB自动加的，不需要用户干预
              - 意向共享锁IS：事务准备给数据行加入共享锁，也就是说一个数据行加共享锁之前必须先取得该表的IS锁
              - 意向排它锁IX：事务准备给数据行加入排它锁，说明事务在一个数据行加排他锁前必须取得该表的IX锁
          - 算法（粒度）：
            - 记录锁：行锁的一种，不过记录锁的范围只是表的某一条记录，记录锁是事务在加锁后锁住的只是表的某一条记录，精准命中，并且命中的字段是唯一索引，加了记录锁之后数据可以避免在查询的时候被修改的重复读问题，页避免了在修改的事务未提交前被其他事务读取的脏读问题。
            - 间隙锁：InnoDB，Next-key锁，属于行锁的一种
              - 我们用范围条件检索数据，并请求共享或排他锁时，InnoDB就会给符合条件的已有数据记录的索引项加锁。这个锁就叫间隙锁
              - 目的：防止幻读，满足恢复和复制的需要
              - 尽量避免使用范围条件
              - 锁定开区间的一段间隔，基于临键锁实现的
              - 存在于非唯一索引
            - 临键锁：Next-key Locks，左开右闭，解决幻读问题
              - 只与非唯一索引列有关，在唯一索引列不存在临键锁
            - 行级锁：InnoDB，锁住的是表中的某一行或者多行记录，其他事务访问同一张表时，只有被锁住的记录不能访问，其他的记录正常访问。
              - 特点：粒度小，加锁比表锁麻烦，不容易冲突，当对于表锁，并发要高很多。会出现死锁
              - 适用于有大量按索引条件并发更新，同时又有查询
            - 表级锁：InnoDB、MyISAM、Memory，锁住整张表。
              - 特点：粒度大，加锁简单，容易冲突。不会出现死锁
              - 适用于查询为主，只有少量按索引条件更新的数据
            - 页级锁：BDB引擎，介于行级锁和表级锁之间。
              - 开销和加锁时间介于表锁和行锁之间，会出现死锁
              - 锁定的粒度介于行锁和表锁之间，并发度一般

- 串行化 serializable
    - 读加读锁、写加写锁

### D：持久性Durability

**事务完成后，数据持久化到磁盘，就算服务器停电，也不会影响到数据**

## 日志
  - redo
    页修改时
    1. 先redo log buffer
    2. redo log 的文件缓存里面（fwrite）
    3. 同步到磁盘（fsync）
  - undo
    事务修改页时
    1. 记undo的redo
    2. 修改数据页
    3. 数据页修改的redo
    4. redo一定要比数据页先持久化到磁盘 


## 主从复制

  1. Master数据库只要发生变化，马上记录到binlog日志文件中
  2. Slave数据库启动一个I/O线程连接Master数据库，请求Master变化的二进制文件
  3. Slave I/O获取到的二进制日志，保存到自己的relay log日志文件中
  4. Slave有一个SQL线程定时检查Relay log是否变化，变化就更新数据。

## 如何保证复制过程中的数据一致性
  - 5.5 引入半同步复制
  - 5.6 引入GTID复制
  - 5.7 引入无损半同步复制

## 备份方式
逻辑备份
  - 优：能够与正在运行的mysql自动协同工作。
  - 缺：备份速度慢，数据量大的时候会锁表
物理备份
  - 缺：只适用MyISAM引擎，无法操作正在运行的mysql
双机热备份：数据量大的时候


## 读写分离，怎么处理从库延迟问题？
  
- 业务让步，接收延迟时间
- 主从同步完成，主库的写才能返回
- 实时性高的查主库
- 接入redis，写的时候增加缓存，读的时候先判断缓存。缓存有，查主库，没有查备库

## Mysql索引的数据结构

B+Tree

- 如果用平衡二叉树，树高太高，索引查询需要访问磁盘，B+tree高度为4或者5
- B tree，B+tree所有的关键字都出现在叶子节点的链表中，而链表中的关键字都是有序的。B tree需要中序遍历二叉树
- B-tree，B-tree节点和数据在一块，如果插入删除时，需要对树进行分裂、合并和转移

## 索引优化

1. 利用覆盖索引进行查询，避免回表操作
2. 禁止三个表join，需要join的字段，数据类型保持绝对一致。多表查询，保证被关联的字段需要有索引。
3. 在varchar字段建立索引，必须指定索引长度，区分度count(distinct left(列名, 索引长度))/count(*)
4. 页面搜索禁止左模糊或者全模糊
5. sql性能优化，至少range，要求时ref级别
   1. const单表中最多只有一个匹配（主键或者唯一索引）
   2. ref是指使用普通索引
   3. range对索引进行范围检索


## 分库分表

- why
  - 当数据量达到几千万时，所花时间变多
- when，什么时候需要分表
  - 单表行数超过500万或者单表容量超过2GB
- How，分库分表策略
  - 垂直拆分
  - 水平拆分
  - 中间件
    - Sharding-sphere（sharding-JDBC）
    - TDDL
    - MyCat


## 参考

[MySQL索引背后的数据结构及算法原理](http://blog.codinglabs.org/articles/theory-of-mysql-index.html)


## 基础


### 连接mysql

```
mysql.exe -h主机地址 -p端口 -u用户名 -p密码
mysql.exe -hlocalhost -p3306 -uroot -proot
```

mysql端口一般都是3306

### 退出

```
exit;
\q;
quit;
```

### mysql服务端架构：

- 数据库管理系统-最外层：DBMS，专门管理服务器端的所有内容
- 数据库-第二层：DB，专门用于存储数据的仓库，可以多个
- 二位数据表-第三层：Table，专门用于存储具体实体的数据
- 字段-第四层：field，具体存储某种类型的数据，实际存储单元

数据库是数据存储的最外层，最大单元


### 创建数据库

```
create database mydatabase;

create database mydatabase2 charset gbk;
```


### 显示数据库

```
show databases;

show databases like 'my%';
```

### 显示数据库创建语句

```
show create database mydatabase;
```

### 选择数据库名字

```
use mydatabase;
```

### 修改数据库

```
# 5.5之前可以修改数据库名，5.5后不允许
alter database mydatabase charset gbk;
```

### 删除数据库

```
drop database mydatabase;
```

### 创建数据表,需要先选数据库

```
use mydatabase2;
create table class(
    --字段名 字段类型
    name varchar(10)
);
# 或
create table mydatabase2.class(
      name varchar(10) 
);
```

### 引擎

```
engine:存储引擎，mysql提供的具体存储数据的方式
    5.5之前默认myisam
    5.5后innodb
charset:字符集，只对当前自己表有效（级别比数据库高）
collate:校对集
create table mydatabase2.class(
      name varchar(10) 
)charset uft8;
```

### 复制已有表结构

```
use test;
show tables;
create table teacher like mydatabase2.teacher;
```

### 显示所有表

```
show tables;
show tables like 't%';
```

### 显示表结构
```
describe 表名
desc 表名
show columns from 表名
describe teacher;

desc teacher;

show columns from teacher;
```

### 显示表创建语句

```
show create table teacher;
```

### 结束符

```
;
\g
\G
```

### 设置表属性

```
--engine,charset,collate
alter table 表名 表选项[=]值
```

### 修改表结构

- 修改表名
```
rename table 旧表名 to 新表名
```

- 修改表选项
```
alter table 表名 表选项 [=] 新值
```
- 新增字段
  - 字段位置：想要存放的位置
  - first 在某个字段之前
  - after 在某个字段之后(默认)
```
alter table 表名 add [column] 新字段名 列类型 [列属性] [位置first/after 字段名]
```

- 修改字段名
```
alter table 表名 change 旧字段名 新字段名 字段类型 [列属性][新位置]
```

- 修改字段类型（属性）
```
alter table 表名 modify 字段名 新类型 [新属性][新位置]
```

- 删除字段
```
alter table 表名 drop 字段名
```
- 删除表结构
  - 可同时删除多个数据表
```
drop table 表名 [,表名2,...]
```

### 增

```
insert into my_teacher(name,age) values ('Jack',30);
insert into my_teacher values ('Jack',30);
```

### 删

```
delete from 表 [where 条件];
```

### 改

```
update 表 set 字段名 = 新值 [where 条件];
```

### 查

```
select * from 表名 [where 条件];
```

### 字符集

ASCII字符集、GB2312字符集、BIG5字符集、GB18030字符集、Unicode字符集

### 查看系统的字符集格式

```
show variables like 'character_set%';


character_set_client 客户端传入,服务器识别客户端的数据
character_set_connection 连接层，客户端和服务器之间的字符集转换
character_set_database 当前数据库存储方式，告诉客户端，服务器返回的数据字符集
character_set_results 结果返回的字符集

```

### 修改服务器端表量的值

```
set 变量名 = 值;

set character_set_client=gbk;
```

### 列类型
|类型|字节数|取数范围|
|---|---|---|
|tinyint| 1 |0-255|
|smallint |2| 0-65535|
|mediumint |3 |
|int |4 |
|bigint |8 |
|float |4 |
|double |8 |
|decimal |自动分配 |
|date |3| 1000-01-01~9999-12-12|
|time |3| -838:59:59~838:59:59|
|dateTime |8| 1000-01-01 00:00:00~ 9999-12-12 12:59:59|
|timestamp  |
|year |1| 1900~2155|
|char |指定| 0-255|
|varchar |自动计算 |
|text |2| 2^16+2|
|tinytext |1| 2^8+1|
|mediumtext |3| 2^24+3|
|longText |4| 2^32+4|

无符号标示标定unsigned
unsigned 无符号标示标定，没有负数

zerofill 从左侧开始填充0，超出长度没办法，低于长度会自动补充

### 浮点

float、double
float 精度大概7位左右

double 精度大概15位

### 定数点decimal

decimal(M,D):M总长度，最大65，D小数部分的长度，最大30


### 记录长度

记录长度不能超过65535个字节
varcahr 在 utf8 中 21844 个字符
varcahr 在 GBK 中 32766 个字符
varchar 会有1-2个字节的额外开销，用来保存数据所占用的空间长度
    如果数据本身小于127，额外开销1个字节，如果大于127，那么就是两个字节

### char 和varchar 区别

- char一定会使用指定的空间，varchar是根据数据来定空间
- char的数据查询效率比varchar高，varchar是需要通过后面的记录数来计算
- 使用
  - 如果确定数据一定是占指定长度--char
  - 如果确定数据有多少--varchar
  - 数据长度超过255个字符，不论是否固定长度--text
  - text:存储普通字符文本，不用刻意选择text类型，系统会自动根据长度来选择合适的文本类型。
  - blob:存储二进制文本（图片、文件）
  - 枚举enum

```
create table my_enum(
    gender enum ('男','女','未知')
)charset utf8;
```

### 存储原理
存储原理：字段上存储的值并不是真正的字符串，而是字符串对应的下标，当系统定义枚举类型的时候，会给枚举的每个元素定义一个下标，下标从1开始。

```
enum(1-'男',2-'女',3-'未知')

select gender + 0 from my_enum;

insert into my_enum(3);
```

- 优点：
  - 规范数据本身，限定只能插入规定的数据项
  - 节省存储空间
  - 集合set，set和enum一样，都是存储下标
  - set('值1','值2','值3'...);

### set

- 1个字节：8个选项
- 2个字节：16个选项
- 3个字节：24个选项
- 8个字节：64个选项

```
create table my_set(
    hobby set('篮球','足球','羽毛球','乒乓球','网球')
)charset utf8;

insert into my_set values('篮球,乒乓球,足球');
```

- 插入的顺序不一样，最终都会变成选项的顺序
- 查询结果为：篮球,足球,乒乓球
- 原理：
  - 从第一个开始进行占位，选择就是1，否则为0，'篮球,乒乓球,足球' -->就是 11010
  - 倒转过来：01011，然后转换成十进制存储2^0+2^1+2^3=1+2+8=11

- enum 单选
- set 复选
- 列属性 ： null、默认值、列描述、主键、唯一键、自动增长
  - null Mysql记录长度为65535个字节，如果一张表有字段允许为null，那么系统就保留一个字节来存储null，最终有效存储长度为65535-1=65534
  -  默认值 default 
  -  列描述：comment 给开发人员进行维护的一个注释说明
  -  主键 primary key， 一张表有且只有一个字段，里面的值具有唯一性
    - 随表创建 
      ```
      create table my_pri(
        username varchar(10),
        primary key (username)
      )charset utf8;
      ```
    - 表后增加 alter table 表名 add primary key(字段);
    - 删除主键 alter table 表名 drop primary key;
    - 主键约束
      - 不能为空
      - 不能重复
    - 分类
      - 业务主键：学号、工号
      - 逻辑主键：自然增长的整型
-  自动增长:auto_increment，给定某个字段该属性后，这列的数据在没有提供确定的数据的时候，系统会根据之前的数据自动增加
    - 原理：系统维护一组数据，用来保存当前使用了自动增长属性的字段，记住当前对应的数据，在给定一个指定的步长
    - 没有给定值，在原始值上加步长的到新的数据
    - 自动增长的触发，给定属性的字段没有提供值
    - 自动增长只适用于数值
    - 一张表只能拥有一个自增长
    - 如果数据插入中没有触发自增长（给定数据），那么自增长不会表现
    - 自增长修改的时候，值可以较大，但是不能比当前已有的自增长的字段的值小
    - 创建
      ```
      create table my_auto(
        id int primary key auto_increment,
        name varchar(10) not null comment '用户名',
        pass varchar(50) not null comment '密码'
      )charset utf8;
      ```
    - 修改，修改自增长的值
      ```
      alter table 表名 auto_increment = 值;
      alter table my_auto auto_increment = 10;
      ```
    - 删除自动增长
      ```
      alter table 表名 auto_increment modify id int;
      ```
    - 初始设置
      ```
      show variables like 'auto_increment%';
      ```
- 唯一键 unique key
    - 用来保证对应的字段中的数据唯一性
    - 唯一键在一张表中可以多个
    - 唯一键允许字段数据为null，null可以为多个（null不参与比较）
    - 在不为空的情况下，不允许重复
    - 创建
      - 方式一：
        ```
        create table my_unique1(
          id int primary key auto_increment,
          username varchar(10) unique key
        )charset utf8;
        ```
      - 方式二：
        ```
        create table my_unique2(
          id int primary key auto_increment,
          username varchar(10),
          unique key(username)
        )charset utf8;
        ```
      - 方式三：
        ```
        create table my_unique2(
          id int primary key auto_increment,
          username varchar(10)
        )charset utf8;
        alter table my_unique3 add unique key(username);
        ```
    - 删除：
      ```
      alter table 表名 drop index 唯一键名字;
      alter table my_unique1 drop index username;
      ```
    - 复合唯一键
      - 唯一键与主键一样可以使用多个字段来共同保证唯一性
      - 一般主键都是单一字段，而其他需要唯一性的内容都是由唯一键来处理

### 主键冲突
  - 插入的时候，不确定数据库中是否已经存在对应的主键
  
  ```
方案一：主键冲突更新
    类似插入数据法，如果插入的过程，主键冲突，那么采用更新方法。
    insert into 表名 [(字段列表)] values(值列表) on duplicate key update 字段=新值;

    insert into my_student values('stu0004','小婷') on duplicate key update stu_name = '小婷';

    方案二：主键冲突替换
        干掉原来的数据，重新插入进去
    replace into 表名 [(字段列表)] values(值列表);

    replace into my_student values('stu0004','小婷');
  ```

###  蠕虫复制

蠕虫复制：一分为二，成倍增加，从已有数据中获取数据，并且将获取到的数据插入到数据表中

```
insert into 表名 [(字段列表)] select */字段名 from 表;

insert into my_simple(name) select name from my_simple;


truncate 重置表，可以重置表选项中的自增长
    先drop，再create
```

- 增
```
insert into 表名 [(字段列表)] values (值列表),(值列表)...;
```
- 改
```
update 表名 set 字段名=新值,字段名=新值... where 判断条件 limit 数量;
```
- 删
```
delete from 表名 where 判断条件 limit 数量;
```
- 查
```
--select选项 all(默认)、distinct 
select (select选项) 字段列表 from 数据源 where 条件 group by 分组 having 条件 order by 排序 limit 限制;
```
- group by 
  - 聚合函数
    - avg()：平均
    - sum()：求和
    - max()：最大值
    - min():最小值
    - group_concat():将分组中指定的字段进行合并(字符串拼接)
- 排序 ：升序/降序  ASC/DESC

### 回溯统计

- 分组进行多分组之后，往上统计的过程中，需要层层上报，将这种层层上报统计的过程称为回溯统计，每一次分组向上统计的过程都会产生一次新的数据，而且当前数据对应的分组字段为NULL
  
```    
group by 字段 [asc/desc] with rollup;

select class_id,count(*) from my_student group by class_id with rollup;

select class_id,gender,count(*) from my_student group by class_id,gender with rollup;
```


### having

```
having ：可以使用聚合函数或者字段别名
    having 是在group by之后，group by 是在where之后，where的时候表示将数据从磁盘拿到内存，where之后的操作都是内存操作。
--group by 之后，
select class_id,count(*) from my_student group by class_id having count(*)>=4;
```

### limit 

```
--offset 偏移量，从哪开始
--length 长度，获取多少条记录
limit offset,length;
```


null 进行任何算术运算都是null

### union

```
union ：联合查询，只要保证字段数一样，不需要每次拿到的数据对应的字段类型一致，永远只保留第一个select语句对应的字段名字
    union 选项： distinct(默认)、all
distinct(默认)，去重去掉完全重复的数据
all，保存所有的结果
select 语句
union [union 选项]
select 语句;

```

### 交叉连接 cross join 

- 原理：
  - 从表1取出每条记录
  - 与表2取出每条记录挨个匹配
  - 没有任何匹配条件，保留所有结果
  - 记录数 = 表1的count(1)* 表2的count(1)
  - 字段数 = 表1的字段数+表2的字段数
    ```
    表1 cross join 表2;
    --结果是笛卡尔积
    --本质：
    from 表1,表2;
    ```



### 内连接 inner join

- 原理：
  - 表1取出记录，去表2中匹配
  - 匹配条件进行匹配
  - 匹配到，保留，继续匹配
  - 匹配不到，继续匹配
  - 全部匹配完毕，结束
    ```
    表1 [inner] join 表2 on 匹配条件;
    ```

### 外连接 outer join
  - 左外连接：left join ，左表是主表
  - 右外连接：right join ，右表是主表
    ```
    表1 left join 表2 on 连接条件;
    主表 left join 从表 on 连接条件; 
    ```

- 原理：
  - 确定主表-表1，左连接就是left join 左边的表是主表
  - 拿主表-表1的每条记录，去匹配表2的每一条记录
  - 如果满足匹配条件，保留，继续匹配
  - 不满足，继续匹配
  - 匹配完毕，如果都不满足，表2的所有字段显示null
- using关键字
  - 连接查询时，使用on的地方用using代替
  - 使用using的前提是对应的两张表的字段名是一样的
  - 使用using关键字，那么对应的同名字段，最终在结果中只会保留一个
    ```
    表1 [inner,left,right] join 表2 using (同名字段列表);
    ```


### 子查询 sub query
    
- 子查询与主查询
- 子查询时潜入到主查询中的
- 子查询辅助主查询，要么作为条件，要么作为数据源
- 子查询可以独立存在，是一条完整的select语句
    
- 子查询分类
  - 功能：
  - 标量子查询：一行一列
  - 列子查询：一列多行
  - 行子查询：一行多列
  - 表子查询：多行多列
  - exists子查询：返回结果1或者0
  - 位置：
  - where子查询：where条件中
  - from子查询：from数据源中




### 整库数据备份与还原

- sql备份
```
mysqldump/mysqldump.exe -hPup 数据库名字[表1 [表2...]] > 备份文件地址
```

- 整库备份
```
mysqldump.exe -hlocalhost -p3306 -uroot -proot mydatabase2 > C:/server/temp/mydatabase2.sql
```

- 多表备份
```
mysqldump.exe -hlocalhost -p3306 -uroot -proot mydatabase2 my_student my_int > C:/server/temp/student_int.sql
```

- 数据还原
```
--方式一
mysql.exe -hPup 数据库 < 文件位置

mysql -uroot -proot mydatabase2 < C:/server/temp/mydatabase2.sql;

--方式二
--必须进入到对应的数据库
source sql 文件位置

source C:/server/temp/student_int.sql;
```

### mysql 用户管理

```
select * from mysql.user;
```

- 创建用户

  ```
  create user 用户名 identified by '明文密码';
  ```

  - 用户：用户名@主机地址
  - 主机地址："/'%'"
  ```
  create user 'user1'@'%' identified by '123456'; 
  ```
  - 谁都可以访问，不需要密码
  ```
  create user user2
  ```
  - 查询用户是否可以使用
  ```
  mysql -uuser2
  ```

- 删除用户

  ```
  drop user user2;
  ```

- 修改用户密码
  - 方式一
  ```
  set password fro 'user1'@'%' = password('654321');
  ```
  - 方式二
  ```
  update mysql.user set password = password('新的明文密码') where user = '' and host = '';
  ```

### 权限

- 数据权限：增删改查（select\update\delete\insert）
- 结构权限：结构操作(create\drop)
- 管理权限：(create user\grant\revoke) grant - 授予权限，revoke-取消权限

- 分配权限
```
grant select on mydb.my_student to 'user1'@'%';
```
- 取消权限
```
revoke all privileges on mydb,my_student from 'user1'@'%';
```
- 刷新权限
```
flush privileges;
```

### 外键

```
[constraint '外键名'] foreign key (外键字段) references 主表 (主键);

create table my_foreign(
    id int primary key not null,
    name varchar(10) not null,
    class_id int,
    foreign key(class_id) references my_class(class_id)
)charset utf8;
```

```
MUL:多索引，
    外键本身就是一个索引
    外键要求外键字段本身也是一种普通索引
```
- 添加外键
```
alter table 从表 add [constraint '外键名'] foreign key(外键字段) references 主表(主键);

alter table my_student add constraint 'student_class_ibfk_1' foreign key(class_id) references my_class(class_id);
```

- 修改外键,外键不能修改，只能先删在增加
```
alter table 从表 drop foreign key 外键名字;
alter table 表名 drop index 索引名字;
```


- 外键的基本要求
  - 外键字段需要保证与关联主表的字段类型完全一致
  - 基本属性也要相同
  - 在表后增加外键，对数据也有一定的要求
  - 外键只能使用innodb存储引擎，myisam不支持

外键约束
```
add foreign key(外键字段) references 主表(主键) on 约束模式;
```

约束模式：
- district:严格模式、默认的，不允许操作
- cascade：级联模式，一起操作，主表变化，从表数据跟着表话
- set null：置空模式，主表变化（删除），从表对应记录设置为空，前提是从表中对应的外键字段允许为空
    
外键约束主要是约束主表操作，从表就是不能插入主表不存在的数据
- 更新级联，删除置空
```
on update cascade
on delete set null;
```

### 创建视图
```
create view 视图名字 as select 指令;

create view student_class_v as 
    select s.* ,c.name 
        from my_student as s 
        left join my_class as c on s.class_id = c.class_id;
```

### 查看视图结构
```
show tables/show create table[view]/desc 视图名字;
```

### 修改视图
```
alter view 视图名字 as 新 select 指令;
```

### 删除视图
```
drop view 视图名字;
```

### 事务

事务：访问并可能更新数据库中各种数据项的一个程序执行单元。
    将用户所做的操作，暂时保存起来，不直接放到数据表，等到用户确认结果之后在进行操作

```
自动事务：autocommit
--关闭自动事务
set autocommit = off;
--开启自动事务
set autocommit = on;
```
- 提交
```
commit;
```
- 回滚

```
rollback;
```

- 开启事务
```
start transaction;
--事务处理
...
```

- 事务提交
```
--rollback;
commit;
```

- 回滚点 
```
savepoint
```

- 增加回滚点
```
savepoint sp1;
```
- 回滚到指定回滚点
```
rollback to sp1;
```

### 变量

- 系统变量

  - 查询所有的系统变量
  ```
  show variables [like 'pattern'];
  ```
  - 查询变量值
  ```
  select @@变量名;
  ```

  - 修改系统变量
  ```
  --局部修改(会话级别)，当前自己客户端连接有效
  set 变量名 = 新值；
  --全局修改，对当前自己客户端无效，需要重新连接才生效
  set global 变量名 = 值;
  set @@global.变量名 = 值;    
  ```

- 会话变量

  ```
  set @变量名 = 值;

  set @name = 'hello world';

  set @age := 50;
  ```

  - 赋值且查看赋值过程

  ```
  select @变量1 :=字段1,@变量2 :=字段2 from 数据表 where 条件;
  select @name :=stu_name ,@age :=stu_age from my_student limit 1;
  ```
  - 只赋值

  ```
  select 字段1,字段2...from 数据源 where 条件 into @变量1,@变量2...;
  select stu_name,stu_age from my_student order by stu_height desc limit into @name,@age;
  select @name,@age;
  ```

- 局部变量
  - 作用范围在begin到end语句块之间
  - 在该语句块中，declare语句专门用于定义局部变量
    - 局部变量是使用declare关键字声明
    - declare语句出现的位置一定是在begin、end之间的(beginend是在大型语句块中使用：函数/存储过程/触发器)
    - 声明语法：declare 变量名 数据类型 [属性];
```
基本语法
if
if 条件表达式 then 
    满足条件要执行的语句;
end if;
```
```
while 
    iterate :迭代，就是以下代码不执行，重新开始循环（continue)
    leave:离开，整个循环终止(break)
while 条件 do
    要循环执行的代码;
end while;
```
```
while 条件 do 
    循环体
end while [标识名字];
```
```
while 条件 do
    if 条件判断 then 
        循环控制;
        iterate/leave 标识名字;
    end if;
    循环体
end while [标识名字];
```

### 函数

|函数名|用途|用法|返回值|
|---|---|---|---|
|char_length() |字符串的字符数| char_length('你好') |2|
|length() |字符串的字节数|(与字符集) length('你好') |6|
|concat() |连接字符串| concat('你好','mysql') |你好mysql|
|inster() |判断目标字符串是否存在| 存在返回所在位置 不存在返回0 select instr('你好1234','2') |4|
|lcase() |全部小写|  ||
|left() |从左侧开始截取，直到指定位置，超出长度，截取所有| left('你好1234','2') |你好|
|ltrim() |消除左边对应的空格|  ||
|mid() |从中间指定位置开始截取| 不指定长度，直到最后 select mid('你好1234',3) |1234|
|now() |当前时间，日期|  ||
|curdate() |当前日期|  ||
|curtime() |当前时间|  ||
|datediff() |判断两个日期之间的天数差距，参数日期必须使用字符串格式| select datediff('2020-10-10','2020-05-05') |158|
|date_add(日期|,|interval 日期数字 type) 时间的增加 type：day/hour/minute/second  ||
|unix_timestamp() |获取时间戳|  ||
|from_unixtime() |指定时间戳转换成对应的日期时间格式|  ||
|abs() |绝对值|  ||
|ceiling() |向上取整|  ||
|floor 向下|取整|  ||
|pow() |求指数，谁的多少次方|  ||
|rand() |获取一个随机数|（0-1）  ||
|round() |四舍五入|  ||
|md5() |md5加密|  ||
|version() |获取版本号|  ||
|database() |显示当前所在数据库|  ||
|UUID() |生成一个唯一标识符|(自增长) 数据整库唯一  ||

### 函数
  
  - 自定义函数是属于用户级别的，只有当前客户端对应的数据库可以使用
  - 可以在不同的数据库下看到对应的函数，但是不能调用
  - 将多行代码集合到一起解决一个重复性问题
  - 函数必须规范返回值，所以函数内部不能使用select指令，只有select 字段 into @变量;（唯一可用） 

### 修改语句结束符

```
delimiter 新符号[可以使用系统非内置即可，如$$]

--修改回语句结束符
delimiter ;
```


### 创建函数

- 修改语句结束符
```
delimiter $$
create function my_func1() returns int
begin
    return 10;
end $$
```

- 修改语句结束符
```
delimiter ;
```

- 最简函数
```
create function mu_func2() return int
return 100;
```

- 查看

```
show function status [like 'pattern'];

show create function 函数名字;
```

- 调用

```
select my_func1();
```

- 删除

```
drop function my_func1();
```

例子：
```
delimiter $$
create function my_sum(num int) returns int
begin
    declare sum int default 0;
    declare count int default 1;
    while count<num do
        if count % 5 !=0  then
            set sum = sum + count;
        end if ;
        set count = count + 1;
    end while;
    return sum;
end $$
delimiter ;
```

- 局部作用域：
  - declare 关键字声明
  - 只能在结构体内部使用
  - 没有任何符号修饰
- 会话作用域
  - 使用@符号定义的变量
  - 使用set关键字
  - 当前用户档次连接有效
  - 会话变量可以在函数内部使用
  - 会话变量可以跨库
- 全局作用域
  - 需要全局符号来定义
    - set global 变量名 = 值;
    - set @@global.变量名 = 值;
    - 比较少用
- 存储过程
  - 大型数据库系统中，为了完成特定功能的sql语句集
  - 第一次遍以后再次调用不需要再次编译--效率高
  - 用户通过指定存储过程的名字并给出参数来执行它。
    
### 存储过程和函数的区别

- 相同：
  - 都是为了重复的执行操作数据库的sql语句集合
  - 存储过程函数都是一次编译，后续执行
- 不同
  - 标识符不同，函数：function 过程：procedure
  - 函数中有返回值，必须返回，过程没有返回值
  - 过程没有返回值类型，不能将结果之间赋值给变量，函数有返回值类型，调用时，除在select中，必须将返回值赋给变量。
  - 函数可以在select语句中直接输用，而过程不行，函数是使用select调用，过程不是

- 创建存储过程
```
--如果过程体只有一条指令，可以省略begin、end
create procedure 过程名字([参数列表]})
begin
    过程体
end
结束符
```
```
--存储过程1-100的和
delimiter $$
create procedure my_pro2()
begin
    --局部变量
    declare i int default 0;
    --会话变量
    set @sum = 0;
    while i<=100 do
        set @sum = @sum + i;
        set i = i + 1;
    end while;
select @sum;
end
$$
delimiter ;
```
    
- 查看过程
```
show procedure status [like 'pattern'];
```

-查询过程创建语句
```
show create procedure 过程名字;
```

- 调用过程
```
call 过程名([实参列表]);

call my_pro2();
```

- 删除过程
```
drop procedure 过程名字;
```

### 行参 in 、out、inout

- in 表示参数从外部传入，可以是直接的数据，也可以是保存数据的变量

- out 表示从存储过程中把数据保存到变量中，交给外部使用，传入必须是变量，如果传入的out变量有值，进入过程之后，会清空

- inout 便是数据可以从外部传入内部使用，同时内部操作后，也可以返还给外部

```
delimiter $$
create procedure my_pro3(in int_1 int, out int_2 int ,inout int_3 int)
begin 
    --查看三个传入尽量的数据的值
    select int_1,int_2,int_3;
    --修改三个变量的值
    set int_1 = 10;
    set int_2 = 100;
    set int_3 = 1000;
    
    select int_1,int_2,int_3;
    select @n1,@n2,@n3;
    
    set @n1='a';
    set @n2='b';
    set @n3='c';
    select @n1,@n2,@n3;
end 
$$
delimiter ;
```

### 触发器

触发器 : 
  - 特殊的存储过程，主要是通过事件进行处罚而被执行的
  - 存储过程是可以通过存储过程名字而被直接调用。
  - trigger 提前给某张表的所有记录绑定一段代码，如果改行的操作满足触发条件，这段代码就会自动执行。
  - 作用
    - 可以在写入数据表前，强制检验或者转换数据---保证数据安全
    - 触发器发生错误时，异动的结果会被撤销--如果触发器执行错误，那么用户执行成功的操作也会撤销，事务安全
    - DDL触发器
  - 优点：
    - 可以通过数据库中国呢相关表实现级联更改
    - 保证数据安全，进行安全校验
  - 缺点
    - 对触发器过分依赖，影响数据库的结构，同时增加维护的复杂程度
    - 造成数据在程序层面不可控
    ```
    create trigger 触发器名字 触发时机 触发事件 on 表 for each row
    begin
    
    end
    ```
- 触发时机
  - before 数据改变之前
  - after 数据改变后  
- 触发事件
  - insert
  - update
  - delete


一张表中，每一个触发时机绑定的触发事件对应的触发器类型智能有一个，一张表只能有一个对应after、insert 触发器
    所以，一张表只能有6个触发器：before insert、before update 、before delete、after insert 、after update 、after delete

```
delimiter $$
create trigger after_inster_order_t after inster on my_orders for each row
begin
update my_goods set inv = inv -1 where id =1;
end 
$$
delimiter ;
```

- 查看触发器
```
show trigger ;
```

-查看触发器的创建语句
```
show create trigger 触发器名字;
```

- 触发触发器
  - 达到条件，比如插入一条语句，更新一条语句、删除一条语句
    
- 删除触发器
```
drop trigger after_insert_order_t;
```

- 触发器应用，可以通过new、old来获取绑定表中对应的记录数据
    old：
    new：



## 面试题

### Mysql框架

- server层
  - 连接器
  - 查询缓存
  - 分析器
  - 优化器
  - 执行器
  - 内置函数
  - 跨存储引擎的功能
- 存储引擎层：数据的存储和提取
  - InnoDB
  - MyISAM

### 一条SQL语句在Mysql框架中的执行流程

1. 应用程序发送到服务端
2. 查询缓存，有，返回给应用程序，没有再往下
3. 查询优化处理，生成执行计划（解析SQL、预处理、优化SQL执行计划）
4. MySQL根据执行计划完成查询
5. 将查询结果返回给客户端



