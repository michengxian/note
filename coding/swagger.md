# swagger

> knife4j 版本



两种访问方式

1. http://localhost:8001/swagger-ui.html
2. http://localhost:8001/doc.html



## 新建项目集成

> 新建一个Springboot项目



### pom.xml

```xml
        <!--springfox-swagger-->
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger2</artifactId>
            <version>2.9.2</version>
        </dependency>

        <dependency>
            <groupId>com.github.xiaoymin</groupId>
            <artifactId>swagger-bootstrap-ui</artifactId>
            <version>1.9.6</version>
        </dependency>
```



其他依赖

```xml
		<dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>

        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.70</version>
        </dependency>
```



### SwaggerConfiguration

> 编写配置文件SwaggerConfiguration

```java
package com.example.demo.config;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUI
public class SwaggerConfiguration {
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.demo"))
                .paths(PathSelectors.any())
                .build();
    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("swagger-bootstrap-ui RESTful APIs")
                .description("swagger-bootstrap-ui")
                .termsOfServiceUrl("http://localhost:8999/")
                .contact("developer@mail.com")
                .version("1.0")
                .build();
    }
}
```



### ApiModelProperty 使用

```java
package com.example.demo.bean;

import com.example.demo.annotations.AutoDocField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class SwaggerReqBean {

    @ApiModelProperty("课程")
    private String name;

    @ApiModelProperty("课程")
    private int age;

    @ApiModelProperty("课程")
    private List<SwaggerSubBean> classList;

}
```



### ApiOperation使用

```java
		@ApiOperation(value = "ApiOperation请求")
    @PostMapping("/apiOperation")
    @ResponseBody
    public String apiOperation(@RequestBody SwaggerReqBean reqBean){
        return "this is post request : "+ JSON.toJSONString(reqBean);
    }
```



### 访问

这时候就可以通过http://localhost:8001/doc.html 访问到页面了



## 旧项目集成

> 旧项目中，对方法和model都有自己的注解，所以需要把自己的注解映射到swagger中

### ApiModelProperty 注解映射

> 两种方式，方式二的`supports` 方法**必须**返回`return true`

#### 方式一

```java
package com.example.demo.config;

import com.example.demo.annotations.AutoDocField;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import springfox.documentation.spi.schema.contexts.ModelPropertyContext;

@Component
@Order(1)
@Primary
public class ApiModelPropertyPropertyBuilderConf extends ApiModelPropertyPropertyBuilder {

    public ApiModelPropertyPropertyBuilderConf(DescriptionResolver descriptions) {
        super(descriptions);
    }

    @Override
    public void apply(ModelPropertyContext context) {
        super.apply(context);
        if (context.getAnnotatedElement().isPresent()) {
            ApiModelProperty model= AnnotationUtils.getAnnotation(context.getAnnotatedElement().get(), ApiModelProperty.class);
            if(model!=null){
                return;
            }
        }
        AutoDocField column=context.getBeanPropertyDefinition().get().getField().getAnnotation(AutoDocField.class);
        if(column!=null){
            if(column.note()!=null && column.note().length()>0){
                context.getBuilder().description(column.note());
                context.getBuilder().required(column.nullable());
            }
        }
    }
}
```



#### 方式二

```java
package com.example.demo.config;

import com.example.demo.annotations.AutoDocField;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.ModelPropertyBuilderPlugin;
import springfox.documentation.spi.schema.contexts.ModelPropertyContext;
import springfox.documentation.spring.web.DescriptionResolver;
import springfox.documentation.swagger.schema.ApiModelPropertyPropertyBuilder;

@Component
@Order(1)
@Primary
public class ApiModelPropertyPropertyBuilderConf implements ModelPropertyBuilderPlugin {
    @Override
    public void apply(ModelPropertyContext modelPropertyContext) {
        if (modelPropertyContext.getAnnotatedElement().isPresent()) {
            ApiModelProperty model= AnnotationUtils.getAnnotation(modelPropertyContext.getAnnotatedElement().get(), ApiModelProperty.class);
            if(model!=null){
                return;
            }
        }
        AutoDocField column=modelPropertyContext.getBeanPropertyDefinition().get().getField().getAnnotation(AutoDocField.class);
        if(column!=null){
            if(column.note()!=null && column.note().length()>0){
                modelPropertyContext.getBuilder().description(column.note());
                modelPropertyContext.getBuilder().required(column.nullable());
            }
        }
    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return true;
    }
}

```



### ApiOperation 注解映射

> 有两种方式，需要注意的是  `supports` 方法**必须**要`return true`

#### 方式一

```java
package com.example.demo.config;

import com.example.demo.annotations.AutoDocMethod;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;

import java.util.List;

@Component
@Order(1)
@Primary
public class ApiOperationPropertyBuilderConf implements OperationBuilderPlugin {
    @Override
    public void apply(OperationContext operationContext) {
        List<ApiOperation> list = operationContext.findAllAnnotations(ApiOperation.class);
        if (list.size() == 0) {
            List<AutoDocMethod> explainList = operationContext.findAllAnnotations(AutoDocMethod.class);
            if (explainList.size() > 0) {
                AutoDocMethod explain = explainList.get(0);
                operationContext.operationBuilder().summary(explain.name());//替换默认值
                operationContext.operationBuilder().deprecated(explain.description());

            }
        }
    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return true;
    }
}
```

#### 方式二：

```java
package com.example.demo.config;

import com.example.demo.annotations.AutoDocMethod;
import com.fasterxml.classmate.TypeResolver;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import springfox.documentation.schema.TypeNameExtractor;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;
import springfox.documentation.swagger.readers.operation.SwaggerOperationResponseClassReader;

import java.util.List;

@Component
@Order(1)
@Primary
public class ApiOperationPropertyBuilderConf extends SwaggerOperationResponseClassReader {
    public ApiOperationPropertyBuilderConf(TypeResolver typeResolver, TypeNameExtractor nameExtractor) {
        super(typeResolver, nameExtractor);
    }

    @Override
    public void apply(OperationContext operationContext) {
        List<ApiOperation> list = operationContext.findAllAnnotations(ApiOperation.class);
        if (list.size() == 0) {
            List<AutoDocMethod> explainList = operationContext.findAllAnnotations(AutoDocMethod.class);
            if (explainList.size() > 0) {
                AutoDocMethod explain = explainList.get(0);
                operationContext.operationBuilder().summary(explain.name());//替换默认值
                operationContext.operationBuilder().deprecated(explain.description());

            }
        }
    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return true;
    }
}
```



### 发现思路

> springfox-swagger-common:2.9.2包中
>
> `ApiModelPropertyPropertyBuilderConf implements ModelPropertyBuilderPlugin`
>
> `SwaggerOperationResponseClassReader implements OperationBuilderPlugin`
>
> 所以可以继承方式，也可以用接口方式



### 注意：

`supports` 返回false时，项目启动，并没有调用`apply`方法





## swagger-ui.html访问404问题

> 在pom.xml中添加



```xml
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>2.9.2</version>
</dependency>
```



## 添加安全登录权限

> 在application.properties中添加

```properties
swagger.production=false
swagger.basic.enable=true
swagger.basic.username=zhangsan
swagger.basic.password=123
```









