# Annotation



## 其他注解



> 作用在代码



### @Override



> 表示重写父类方法，如果父类没有，会报错



```java
@Override
```





### @Deprecated



> 表示过时方法



```java
@Deprecated
```



### @SuppressWarnings



> 忽略警告



```java
@SuppressWarning
```





## 元注解



> 作用于其他注解





### @Target



> 注解作用目标



```java
@Target({ElementType.TYPE,ElementType.METHOD})
```



#### ElementType



- TYPE 

  - 接口、类、枚举、注解

- FIELD

  - 字段、枚举的常量

- METHOD

  - 方法

- PARAMETER

  - 方法参数

- CONSTRUCTOR

  - 构造函数

- LOACL_VARIABLE

  - 局部变量

- ANNOTATION_TYPE

  - 注解

- PACKAGE

  - 包

- TYPE_PARAMETER

  - 任何声明类型的地方

- TYPE_USE

  - 任意使用的地方

  

### @Reterntion

> 注解保留位置



```java
@Retention(RetentionPolicy.RUNTIME)
```



#### @RetentionPolicy



- SOURCE
  - 在源文件中有效
- CLASS
  - 在class文件中有效
- RUNTIME
  - 在运行时有效



### @Documented



> 描述其他类型的annotation应该被作为被标注的程序成员公共API，可以被javadoc类工具文档化



```java
@Documented
```



### @Inherited

> 子类可以继承父类中的该注解



```java
@Inherited
```







## 自定义注解使用



### CacheAnnotation

```java
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CacheAnnotation {

    String cacheName() default "";

    int expire() default 60;
}
```



### CacheConfig

```java
@Configuration
@ConfigurationProperties("mi.cache.config")
@Data
public class CacheConfig {

    private String cachePackage = "*";

    private int expireTime = 60;

    private int maxSize = 100;

}
```



### CacheMap

```java
@Component
public class CacheMap {

    private ConcurrentMap<String,CacheBean> concurrentMap;

    @Bean
    public CacheMap map(){
        CacheMap cacheMap = new  CacheMap();
        cacheMap.concurrentMap = new ConcurrentHashMap<>();
        return cacheMap;
    }

    public boolean put(String key , Object value,int time){
        try {
            Long startTime = System.currentTimeMillis();
            CacheBean bean = new CacheBean();
            if (this.concurrentMap.get(key)!=null){
                bean = this.concurrentMap.get(key);
            }
            else {
                bean.setStartDate(startTime);
                bean.setEndDate(startTime+time*1000);
                bean.setValue(value);
            }
            this.concurrentMap.put(key, bean);
            return true;
        }
        catch (Exception exception){
            return false;
        }
    }

    public Object get(String key){
        CacheBean bean = concurrentMap.get(key);
        if (bean!=null && bean.getEndDate()>System.currentTimeMillis()){
            return bean.getValue();
        }
        else {
            concurrentMap.remove(key);
            return null;
        }
    }


    public int size(){
        Long now = System.currentTimeMillis();
        concurrentMap.forEach((key,value) ->{
            if (value.getEndDate() <= now){
                concurrentMap.remove(key);
            }
        });
        return concurrentMap.size();
    }
}
```

### CacheBean

```java
@Data
public class CacheBean {

    private Long startDate;

    private Long endDate;

    private Object value;
}
```



### CacheAspect

```java
@Aspect
@Component
public class CacheAspect {

    @Autowired
    private CacheConfig cacheConfig;

    @Autowired
    private CacheMap map;

    @Around("@annotation(com.mi.learn.aspect.demo.cache.CacheAnnotation)")
    public Object aroundCache(ProceedingJoinPoint pjp){
        MethodSignature methodSignature = (MethodSignature) pjp.getSignature();
        Method method = methodSignature.getMethod();
        CacheAnnotation cacheAnnotation = method.getAnnotation(CacheAnnotation.class);

        Object res = null;
        try {
            res = pjp.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        String key = method.getDeclaringClass().getTypeName()+"."+method.getName();
        String cacheName = StringUtils.isNotBlank(cacheAnnotation.cacheName())?cacheAnnotation.cacheName():key;
        int time = cacheAnnotation.expire()>cacheConfig.getExpireTime()?cacheConfig.getExpireTime():cacheAnnotation.expire();
        int size = map.size();

        if (size+1 > cacheConfig.getMaxSize()){
            throw new RuntimeException("缓存太大，请稍后在试");
        }
        String p = method.getDeclaringClass().getName()+"."+method.getName();
        boolean flag = Pattern.matches(cacheConfig.getCachePackage() ,p);
        if (flag){
            map.put(cacheName,res,time);
        }
        return res;
    }

}
```

### application.properties

```properties
server.port=8001

mi.cache.config.cachePackage=.*
mi.cache.config.expireTime=100
mi.cahce.config.maxSize=100
```



### DemoTestController

```java
@RequestMapping(value = "/test",method = RequestMethod.GET)
@CacheAnnotation
public String  test(){
    return "success";
}
```













