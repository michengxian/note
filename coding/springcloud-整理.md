# spring-cloud





## Eureka-注册中心



1. 新建一个普通的maven项目spring-cloud

2. 建立三个module，分别为eureka-6001、eureka-6002、eureka-6003

3. [修改spring-cloud的pom.xml](#spring-cloud的pom.xml)

4. 修改eureka

   1. 修改`Eureka6001Application`，引入`@EnableEurekaServer`
   2. [修改`application.properties`](#eureka-6001的application.properties)
   3. [修改pom.xml](#eureka-6001的pom.xml)
   4. `Eureka6002`、`Eureka6003`和`Eureka6001`的配置一样

   

**spring-cloud的pom.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <!--父依赖-->
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.3.5.RELEASE</version>
        <relativePath/>
    </parent>

    <!--项目配置-->
    <groupId>org.example</groupId>
    <artifactId>spring-cloud</artifactId>
    <version>1.0-SNAPSHOT</version>

    <!--公共版本配置-->
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>
        <spring.cloud.version>Hoxton.SR9</spring.cloud.version>
    </properties>

    <!--子模块-->
    <modules>
        <module>eureka-6001</module>
        <module>eureka-6002</module>
        <module>eureka-6003</module>
    </modules>

    <!--公共依赖配置-->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring.cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <!--依赖-->
    <dependencies>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.junit.vintage</groupId>
                    <artifactId>junit-vintage-engine</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>

    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

</project>
```



**eureka-6001的application.properties**

```properties
# 对应服务的端口
server.port=6001

spring.application.name=eureka-server
eureka.client.register-with-eureka=false
eureka.client.fetch-registry=false

# 三台注册服务器，引入非本机的其他两台
eureka.client.serviceUrl.defaultZone=http://eureka-server:6001/eureka/,http://eureka-server:6002/eureka/
```



**eureka-6001的pom.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <!--父依赖-->
    <parent>
        <artifactId>spring-cloud</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>

    <!--本项目配置-->
    <groupId>com.example</groupId>
    <artifactId>eureka-6001</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>eureka-6001</name>
    <description>Demo project for Spring Boot</description>


    <dependencies>
      
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
      
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
        </dependency>

    </dependencies>

</project>
```



**访问eureka**

http://eureka-server:6003



### 问题

**1. 如果访问不了eureka-server，需要修改hosts文件**

mac下：` /etc/hosts `目录下，增加

```host
127.0.0.1	eureka-server
```

即可





## Feign调用

1. 新建两个module，feign-server-8003、feign-client-8004

2. 如果有多个feign-server，那么feign-client会分别去调用不同的feign-server，负载均衡

### feign-server

pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>spring-cloud</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <groupId>com.example</groupId>
    <artifactId>feign-server-8003</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>feign-server-8003</name>
    <description>Demo project for Spring Boot</description>

    <properties>
        <java.version>1.8</java.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
    </dependencies>
  
</project>
```

application.properties

```properties
server.port=8003

spring.application.name=feign-server

eureka.client.serviceUrl.defaultZone=http://eureka-server:6001/eureka/,http://eureka-server:6002/eureka/,http://eureka-server:6003/eureka/
```

FeignServer8003Application

```java
@EnableEurekaClient
```

TestController

```java
package com.example.feignserver8003.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @RequestMapping("/feignServer")
    public String feignServer(@RequestParam("name") String name){
        System.out.println("FeignServer8003 TestController name : " + name);
        return "FeignServer8003 TestController name : " + name ;
    }

}
```

### feign-client

pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>spring-cloud</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <groupId>com.example</groupId>
    <artifactId>feign-client-8004</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>feign-client-8004</name>
    <description>Demo project for Spring Boot</description>

    <properties>
        <java.version>1.8</java.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>

        <!--feign-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-openfeign</artifactId>
        </dependency>

    </dependencies>

</project>
```

application.properties

```properties
spring.application.name=feign-client

server.port=8004

eureka.client.serviceUrl.defaultZone=http://eureka-server:6001/eureka/,http://eureka-server:6002/eureka/,http://eureka-server:6003/eureka/
```

FeignClient8004Application

```java
@EnableEurekaClient
@EnableFeignClients
```

FeignService

```java
package com.example.feignclient8004.server;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("feign-server")
public interface FeignService {

    @RequestMapping("/feignServer")
    String feignServer(@RequestParam("name") String name);

}
```

TestController

```java
package com.example.feignclient8004.controller;

import com.example.feignclient8004.server.FeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private FeignService service;

    @RequestMapping("/feignClient")
    public String feignClient(@RequestParam("name") String name){
        String res = service.feignServer(name);
        System.out.println("FeignServer8003 TestController res : " + res);
        return "FeignServer8003 TestController res : " + res ;
    }
}
```







## Hystrix熔断

需要feign-server

新建hystrix-feign-client

如果有多个feign-server，则会负载均衡



### hystrix-feign-client

**pom.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>spring-cloud</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <groupId>com.example</groupId>
    <artifactId>hytrix-feign-server-8006</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>hytrix-feign-server-8006</name>
    <description>Demo project for Spring Boot</description>


    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>

        <!--feign-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-openfeign</artifactId>
        </dependency>

    </dependencies>

</project>
```



**application.properties**

```properties
server.port=8006

spring.application.name=hystrix-feign-client

eureka.client.serviceUrl.defaultZone=http://eureka-server:6001/eureka/,http://eureka-server:6002/eureka/,http://eureka-server:6003/eureka/

hystrix.metrics.enabled=true

# 默认feign的hystrix为关闭状态
feign.hystrix.enabled=true
```

**HystrixFeignClient8006Application**

```java
@EnableEurekaClient
@EnableFeignClients
```



**HystrixFailBack**

```java
package com.example.hystrixfeignclient8006.failback;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name= "feign-server", fallback = HystrixFailBackImpl.class)
public interface HystrixFailBack {

    @RequestMapping("/feignServer")
    String feignServer(@RequestParam("name") String name);
}
```



**HystrixFailBackImpl**

```java
package com.example.hystrixfeignclient8006.failback;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

@Component
public class HystrixFailBackImpl implements HystrixFailBack {

    @Override
    public String feignServer(@RequestParam(value = "name") String name) {
        return "hello " + name + ", i am fallback massage";
    }
}
```



**TestController**

```java
package com.example.hystrixfeignclient8006.controller;

import com.example.hystrixfeignclient8006.failback.HystrixFailBack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private HystrixFailBack service;

    @RequestMapping("/hystrixFeignServer")
    public String hystrixFeignServer(@RequestParam("name") String name){
        String res = service.feignServer(name);
        System.out.println("HystrixFeignServer8006 TestController res : " + res);
        return "HystrixFeignServer8006 TestController res : " + res ;
    }
}
```





##Hystrix-dashboard熔断看板







###hystrix-dashboard

**pom.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>spring-cloud</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <groupId>com.example</groupId>
    <artifactId>hystrix-dashboard-5001</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>hystrix-dashboard-5001</name>
    <description>Demo project for Spring Boot</description>


    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-hystrix-dashboard</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-openfeign</artifactId>
        </dependency>

    </dependencies>

</project>
```



**application.properties**

```properties
server.port=5001

spring.application.name=hystrix-dashboard

eureka.client.service-url.defaultZone=http://eureka-server:6001/eureka/,http://eureka-server:6002/eureka/,http://eureka-server:6003/eureka/

feign.hystrix.enabled=true
hystrix.dashboard.proxy-stream-allow-list=*

management.endpoints.web.exposure.include=hystrix.stream
```



**HystrixDashboard5001Application**

```java
package com.example.hystrixdashboard5001;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableHystrixDashboard
@EnableCircuitBreaker
public class HystrixDashboard5001Application {

    public static void main(String[] args) {
        SpringApplication.run(HystrixDashboard5001Application.class, args);
    }

    @Bean
    public ServletRegistrationBean getServlet() {
        HystrixMetricsStreamServlet streamServlet = new HystrixMetricsStreamServlet();
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(streamServlet);
        registrationBean.setLoadOnStartup(1);
        registrationBean.addUrlMappings("/hystrix.stream");//访问路径
        registrationBean.setName("HystrixMetricsStreamServlet");
        return registrationBean;
    }
}
```

**HystrixFailBack**

```java
package com.example.hystrixdashboard5001.failback;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "feign-server", fallback = HystrixFailBackImpl.class)
public interface HystrixFailBack {

    @RequestMapping("/feignServer")
    String feignServer(@RequestParam("name") String name);
}
```



**HystrixFailBackImpl**

```java
package com.example.hystrixdashboard5001.failback;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

@Component
public class HystrixFailBackImpl implements HystrixFailBack {

    @Override
    public String feignServer(@RequestParam(value = "name") String name) {
        return "hello " + name + ", i am fallback massage";
    }
}
```

**TestController**

```java
package com.example.hystrixdashboard5001.controller;

import com.example.hystrixdashboard5001.failback.HystrixFailBack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private HystrixFailBack service;

    @RequestMapping("/hystrixDashboard")
    public String hystrixDashboard(@RequestParam("name") String name) {
        String res = service.feignServer(name);
        System.out.println("HystrixDashboard5001Application TestController res : " + res);
        return "HystrixDashboard5001Application TestController res : " + res;
    }

}
```



### 使用

1. 访问http://localhost:5001/hystrix.stream，ping：不为空

![image-20201126153900307](/Users/biostime/Desktop/工作/每月总结/java/image-20201126153900307.png)

2. 进入http://localhost:5001/hystrix，并输入http://localhost:5001/hystrix.stream

![image-20201126154101332](/Users/biostime/Desktop/工作/每月总结/java/image-20201126154101332.png)

3. 访问http://localhost:5001/hystrixDashboard?name=hhh

4. 面板显示效果

   ![image-20201126154142127](/Users/biostime/Desktop/工作/每月总结/java/image-20201126154142127.png)





## Zuul

### zuul-9010

**pom.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>spring-cloud</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <groupId>com.example</groupId>
    <artifactId>zool-9010</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>zuul-9010</name>
    <description>Demo project for Spring Boot</description>


    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-zuul</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>

    </dependencies>

</project>
```



**application.properties**

```properties

server.port=9010

spring.application.name=zuul

zuul.routes.demo.path=/demo/**
# 转发本项目
#zuul.routes.baidu.service-id=forward:/
# 路由到项目的路径
zuul.routes.demo.url=http://localhost:8003/
# 忽略这个目录下的路径
#zuul.ignored-patterns=/demo/**

spring.cloud.loadbalancer.ribbon.enabled=false
spring.main.allow-bean-definition-overriding=true
eureka.client.serviceUrl.defaultZone=http://eureka-server:6001/eureka/,http://eureka-server:6002/eureka/,http://eureka-server:6003/eureka/
```



**Zuul9010Application**

```java
@EnableZuulProxy
@EnableEurekaClient
```



**TokenFilter**

```java
package com.example.zuul9010.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class TokenFilter extends ZuulFilter {

    private final Logger logger = LoggerFactory.getLogger(TokenFilter.class);

    @Override
    public String filterType() {
        return "pre"; // 可以在请求被路由之前调用
    }

    @Override
    public int filterOrder() {
        return 0; // filter执行顺序，通过数字指定 ,优先级为0，数字越大，优先级越低
    }

    @Override
    public boolean shouldFilter() {
        return true;// 是否执行该过滤器，此处为true，说明需要过滤
    }

    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();

        logger.info("--->>> TokenFilter {},{}", request.getMethod(), request.getRequestURL().toString());

        String token = request.getParameter("token");// 获取请求的参数

        if (StringUtils.isNotBlank(token)) {
            ctx.setSendZuulResponse(true); //对请求进行路由
            ctx.setResponseStatusCode(200);
            ctx.set("isSuccess", true);
            return null;
        } else {
            ctx.setSendZuulResponse(false); //不对其进行路由
            ctx.setResponseStatusCode(400);
            ctx.setResponseBody("token is empty");
            ctx.set("isSuccess", false);
            return null;
        }
    }

}
```



**注意**

TokenFilter中加入@Component

如果不加的话，需要在Zuul9010Application中加入@Bean

```java
@Bean
public TokenFilter tokenFilter() {
  return new TokenFilter();
}
```



### 使用

启动eureka、feign-server、zuul

分别访问

http://localhost:9010/demo/feignServer?name=kk这个会报token is empty

http://localhost:9010/demo/feignServer?name=123&token=kk访问正常



**增强**

ErrorHandlerController

```java
package com.example.zuul9010.controller;


import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ErrorHandlerController  implements ErrorController {

    /**
     * 出异常后进入该方法，交由下面的方法处理
     */
    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping("/error")
    public String error() {
        return "出现异常";
    }
}
```



这时访问http://localhost:9010/demo111/feignServer?name=kk，项目中并没有demo111这个路径，所以会异常，但是会被ErrorHandlerController截取











## Gateway-网关

拦截所有的请求

1. [修改pom.xml](#gateway9001的pom.xml)
2. 修改`application.properties`，更改下端口`server.port=9001`即可
3. [修改`Gateway9001Application`](#Gateway9001Application)



### gateway9001的pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
  	<!--父依赖-->
    <parent>
        <artifactId>spring-cloud</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <groupId>com.example</groupId>
    <artifactId>gateway-9001</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>gateway-9001</name>
    <description>Demo project for Spring Boot</description>

    <dependencies>
        <!--引入网关-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-gateway</artifactId>
        </dependency>

        <!--添加服务熔断依赖-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
        </dependency>
    </dependencies>

</project>
```



### Gateway9001Application
**方式一:不添加熔断信息，只拦截**

```java
    @Bean
    public RouteLocator myRoutes(RouteLocatorBuilder builder) {
        String httpUri = "http://localhost:8001";
        return builder.routes()
                .route(p -> p
                        .path("/get*")
                        .filters(f -> f.addRequestParameter("name", "World"))
                        .uri("http://localhost:8001/"))
                .build();
    }
```

**方式二:添加熔断信息，并拦截**

```java
@SpringBootApplication
@RestController
@EnableConfigurationProperties(UriConfiguration.class)
public class Gateway9001Application {

    public static void main(String[] args) {
        SpringApplication.run(Gateway9001Application.class, args);
    }
		
		@Bean
    public RouteLocator myRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(p -> p
                        .path("/get")
                        .filters(f -> f.addRequestParameter("name", "World").
                                hystrix(config -> config.setName("myFallBack").setFallbackUri("forward:/fallback"))
                        )
                        .uri("http://localhost:8001/"))
                .build();
    }

    @RequestMapping("/fallback")
    public Mono<String> fallback() {
        return Mono.just("fallback1111");
    }
}
```

**方式三:**

```java
package com.example.gateway9001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@SpringBootApplication
@RestController
@EnableConfigurationProperties(UriConfiguration.class)
public class Gateway9001Application {

    public static void main(String[] args) {
        SpringApplication.run(Gateway9001Application.class, args);
    }
  
    @Bean
    public RouteLocator myRoutes(RouteLocatorBuilder builder,UriConfiguration uriConfiguration) {
        String httpUri = uriConfiguration.getHttpbin();
        return builder.routes()
                .route(p -> p
                        .path("/get")
                        .filters(f -> f.addRequestParameter("name", "World").
                                hystrix(config -> config.setName("myFallBack").setFallbackUri("forward:/fallback"))
                        )
                        .uri(httpUri))
                .build();
    }

    @RequestMapping("/fallback")
    public Mono<String> fallback() {
        return Mono.just("fallback1111");
    }
}

@ConfigurationProperties
class UriConfiguration {

    private String httpbin = "http://localhost:8001";

    public String getHttpbin() {
        return httpbin;
    }

    public void setHttpbin(String httpbin) {
        this.httpbin = httpbin;
    }
}
```



## Config-配置中心

1. 新建两个`module`，`config-7001`、`config-client-8002`
2. [修改`config-7001`的pom.xml](#config-7001的pom.xml)
3. 把`config-7001`的`application.properties`删除，创建`bootstrap.properties`，**bootstrap的级别比application要高，会先加载bootstrap**
4. [修改`config-7001`的`bootstrap.properties`](#config-7001的bootstrap.properties)
5. 修改`Config7001Application`,引入`@EnableConfigServer`、`@EnableEurekaClient`
6. [修改`config-client-8002`的`pom.xml`](#config-client-8002的pom.xml)
7. [修改`config-client-8002`的`bootstrap.properties`](#config-client-8002的bootstarp.properties)，创建`bootstrap.properties`的方法与第3点一致
   1. **注意`spring.application.name`与`spring.cloud.config.profile`在对应的配置中心要有相应的文件**
8. [修改`ConfigClient8002Application`](#ConfigClient8002Application)



**config-7001的pom.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>spring-cloud</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <groupId>com.example</groupId>
    <artifactId>config-7001</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>config-7001</name>
    <description>Demo project for Spring Boot</description>

    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-config-server</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
    </dependencies>

</project>
```



**config-7001的bootstrap.properties**

```properties
spring.application.name=server-config
server.port=7001
spring.cloud.config.server.git.uri=https://github.com/michengxian/SpringcloudConfig
spring.cloud.config.server.git.searchPaths=config
spring.cloud.config.label=master
spring.cloud.config.server.git.username=
spring.cloud.config.server.git.password=
eureka.client.service-url.defaultZone=http://eureka-server:6001/eureka/,http://eureka-server:6002/eureka/,http://eureka-server:6003/eureka/
```



**config-client-8002的pom.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>spring-cloud</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <groupId>com.example</groupId>
    <artifactId>config-client-8002</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>config-client-8002</name>
    <description>Demo project for Spring Boot</description>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-config</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
    </dependencies>

</project>
```



**config-client-8002的bootstarp.properties**

```properties
server.port=8002
# 需要有对应的配置文件bootstrap-dev.properties
spring.application.name=bootstrap

eureka.client.service-url.defaultZone=http://eureka-server:6001/eureka/,http://eureka-server:6002/eureka/,http://eureka-server:6003/eureka/

spring.cloud.config.label=master
spring.cloud.config.profile=dev
spring.cloud.config.uri=http://localhost:7001/
```



**ConfigClient8002Application**

```java
package com.example.configclient8002;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@RestController
public class ConfigClient8002Application {

    public static void main(String[] args) {
        SpringApplication.run(ConfigClient8002Application.class, args);
    }

    @Value("${foo}")
    String foo;

    @Value("${name}")
    String name;

    @RequestMapping(value = "/config")
    String config(){
        System.out.println("server-8002 ConfigController config foo:"+foo + " name:"+name);
        return "server-8002 ConfigController config foo:"+foo + " name:"+name;
    }

    @RequestMapping(value = "/serviceConfigClient")
    public String serviceConfigClient() {
        return foo;
    }

    @Value("${spring.cloud.config.uri}")
    String uri;

    @RequestMapping(value = "/getUri")
    public String getUri() {
        return uri;
    }

}
```



## Bus-消息总线



### 启动Rabbitmq

```bash
# 进入目录
cd /usr/local/Cellar/rabbitmq/3.7.16/sbin

# 启动rabbitmq
./rabbitmq-server
```



### config 服务端

**bootstrap.properties**

```properties
spring.application.name=server-config
server.port=7001
spring.cloud.config.server.git.uri=https://github.com/michengxian/SpringcloudConfig
spring.cloud.config.server.git.searchPaths=config
spring.cloud.config.label=master
spring.cloud.config.server.git.username=
spring.cloud.config.server.git.password=
eureka.client.service-url.defaultZone=http://eureka-server:6001/eureka/,http://eureka-server:6002/eureka/,http://eureka-server:6003/eureka/


spring.rabbitmq.host=localhost
spring.rabbitmq.port=5672
spring.rabbitmq.username=guest
spring.rabbitmq.password=guest
management.endpoints.web.exposure.include=bus-refresh
```



**pom.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>spring-cloud</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <groupId>com.example</groupId>
    <artifactId>config-7001</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>config-7001</name>
    <description>Demo project for Spring Boot</description>

    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-config-server</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>

        <!--spring cloud bus-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bus-amqp</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>

    </dependencies>

</project>
```



**Config7001Application**

```java
@EnableConfigServer
@RestController
@EnableEurekaClient
```



### config-client客户端

**bootstrap.properties**

```properties
server.port=8002
# 需要有对应的配置文件bootstrap-dev.properties
spring.application.name=bootstrap

eureka.client.service-url.defaultZone=http://eureka-server:6001/eureka/,http://eureka-server:6002/eureka/,http://eureka-server:6003/eureka/

spring.cloud.config.label=master
spring.cloud.config.profile=dev
spring.cloud.config.uri=http://localhost:7001/


spring.rabbitmq.host=localhost
spring.rabbitmq.port=5672
spring.rabbitmq.username=guest
spring.rabbitmq.password=guest
spring.cloud.bus.trace.enabled=true
```



**pom.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>spring-cloud</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <groupId>com.example</groupId>
    <artifactId>config-client-8002</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>config-client-8002</name>
    <description>Demo project for Spring Boot</description>


    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-config</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>


        <!--spring cloud bus-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bus-amqp</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>

    </dependencies>

</project>
```



**TestController**

```java
package com.example.configclient8002.controller;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class TestController {

    @Value("${foo}")
    String foo;

    @Value("${name}")
    String name;

    @RequestMapping(value = "/config")
    String config(){
        System.out.println("server-8002 ConfigController config foo:"+foo + " name:"+name);
        return "server-8002 ConfigController config foo:"+foo + " name:"+name;
    }


    @RequestMapping(value = "/serviceConfigClient")
    public String serviceConfigClient() {
        return foo;
    }


    @Value("${spring.cloud.config.uri}")
    String uri;

    @RequestMapping(value = "/getUri")
    public String getUri() {
        return uri;
    }
}
```



**ConfigClient8002Application**

```java
@EnableEurekaClient
```



### 使用方式

1. 请求http://localhost:8002/config，记住当前配置
2. 去配置中心，修改对应的配置文件https://github.com/michengxian/SpringcloudConfig
3. **post**请求http://localhost:7001/actuator/bus-refresh，必须post请求，必须这个链接
4. 刷新http://localhost:8002/config，就会展示修改后的







## admin监控



创建两个module，admin-5010、admin-client-5011



### admin-5010



**pom.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>spring-cloud</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <groupId>com.example</groupId>
    <artifactId>admin-5010</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>admin-5010</name>
    <description>Demo project for Spring Boot</description>

    <properties>
        <spring-boot-admin.version>2.3.0</spring-boot-admin.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>

        <dependency>
            <groupId>de.codecentric</groupId>
            <artifactId>spring-boot-admin-starter-server</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>

    </dependencies>


    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>de.codecentric</groupId>
                <artifactId>spring-boot-admin-starter-server</artifactId>
                <version>${spring-boot-admin.version}</version>
            </dependency>
            <dependency>
                <groupId>de.codecentric</groupId>
                <artifactId>spring-boot-admin-starter-client</artifactId>
                <version>${spring-boot-admin.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

</project>
```



**application.properties**

```properties
server.port=5010

spring.application.name=admin

management.endpoints.web.exposure.include=*
management.endpoint.health.show-details=always

eureka.client.service-url.defaultZone=http://eureka-server:6001/eureka/,http://eureka-server:6002/eureka/,http://eureka-server:6003/eureka/
```

**Admin5010Application**

```java
@EnableAdminServer
@EnableEurekaClient
```



### admin-client-5011

**pom.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <artifactId>spring-cloud</artifactId>
        <groupId>org.example</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <groupId>com.example</groupId>
    <artifactId>admin-client-5011</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <name>admin-5011</name>
    <description>Demo project for Spring Boot</description>

    <properties>
        <spring-boot-admin.version>2.3.0</spring-boot-admin.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>

        <dependency>
            <groupId>de.codecentric</groupId>
            <artifactId>spring-boot-admin-starter-client</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>de.codecentric</groupId>
                <artifactId>spring-boot-admin-starter-server</artifactId>
                <version>${spring-boot-admin.version}</version>
            </dependency>
            <dependency>
                <groupId>de.codecentric</groupId>
                <artifactId>spring-boot-admin-starter-client</artifactId>
                <version>${spring-boot-admin.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

</project>
```



**application.properties**

```properties
server.port=5011
spring.application.name=admin-client
eureka.client.service-url.defaultZone=http://eureka-server:6001/eureka/,http://eureka-server:6002/eureka/,http://eureka-server:6003/eureka/

management.endpoints.web.exposure.include=*
management.endpoint.health.show-details=always
```



**AdminClient5011Application**

```java
@EnableEurekaClient
```



### 使用

进入http://10.50.101.123:5010

![image-20201127151926639](/Users/biostime/Desktop/工作/每月总结/java/image-20201127151926639.png)













































