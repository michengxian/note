# 注解和反射

## 注解



- Annotation 是从JDK5.0开始的

- 不是程序本身，可以对程序做出解释

- 可以被其他程序读取

- 格式：@注释名



### 内置注解



```bash
@Override 重写父类中方法的声明
@Deprecated	修饰方法、属性、类，表示过时方法
@SuppressWarnings	抑制编译的警告信息
	添加一个参数才能使用，这些参数都是定义好的
	@SuppressWarnings("all")
	@SuppressWarnings("unchecked")
```



### 元注解

```bash
@Target	注解的使用范围
@Retention	表示需要在什么级别保存该注解(source<class<RUNTIME)
@Document	该注解包含在javadoc中	
@Inherited	子类可以继承父类中的该注解
```



### 自定义注解

```bash
@interface 声明一个注解	public @interface 注解名{定义内容}
每一个方法实际上是声明一个配置参数
方法的名称就是参数的名称
返回值类型就是参数的类型，只有 基本类型、Class、String、enum
default声明参数的默认值，空字符串、0作为默认值
只有一个参数，一般用value，可以省略不写
注解元素必须要有值
```



***

## 反射

> 是Java被视为动态语言的关键，反射机制允许程序在执行期接触与Reflection API取得任何类的内部信息，并能直接操作任意对象的内部属性及方法。

```java
Class c = Class.forName("java.long.String");
```

加载完类之后，在堆内存的方法区中就产生了一个Class类型的对象，这个对象就包含了完整的类的结构信息，我们可以看到类的结构。我们称之为反射

- 正常：引入包类名称-->new实例化-->取得实例化对象
- 反射：实例化对象-->getClass()方法-->得到包类名称



反射机制提供的功能

- 在运行时判断任意一个对象所属的类
- 在运行时构造任意一个类的对象
- 在运行时判断任意一个类所具有的成员变量和方法
- 在运行时获取泛型信息
- 在运行时调用任意一个对象的成员变量和方法
- 在运行时处理注解
- 生成动态代理



> 反射的优缺点

优点：可以实现动态创建对象和编译，体现很大的灵活性

缺点：对性能有影响。使用反射基本上是一种解释操作，告诉JVM，我们希望做什么并且他满足我们的要求，这类操作总是慢于直接执行相同的操作。



### Class类

- Class 本身也是一个类
- Class 对象只能由系统建立对象
- 一个加载的类在JVM中只会有一个Class实例
- 一个Class对象对应的是一个加载到JVM中的一个.class文件
- 每个类的实例都会记得自己是由那个Class实例所生成的
- 通过Class可以完整的得到一个类中的所有被加载的结构
- Class类是Reflection的根源，针对任何你想动态加载、运行的类，唯有先获的相应的Class对象



| 方法名                                  | 功能说明                                                     |
| --------------------------------------- | ------------------------------------------------------------ |
| static ClassforName(String name)        | 返回指定类名name的Class对象                                  |
| Object newInstance()                    | 调用缺省构造函数，返回Class对象的一个实例                    |
| getName()                               | 返回此Class对象所表示的实体（类，接口，数组类或者void）的名称 |
| Class getSuperClass()                   | 返回当前Class对象的父类的Class对象                           |
| Class[] getinterfaces()                 | 获取当前Class对象的接口                                      |
| ClassLoader getClassLoader()            | 返回该类的类加载器                                           |
| Constructor[] getConstructors()         | 返回一个包含某些Constructor对象的数组                        |
| Method getMothed(String name,Class...T) | 返回一个Method对象，此对象的形参类型为paramType              |
| Field[] getDeclaredFields()             | 返回Field对象的一个数组                                      |



```java
//通过类的class属性获取，最安全可靠，程序性能最高
Class claszz = Person.class;

//通过某个类的实例，获取Class对象
Class clazz = person.getClass();

//类的全类名，获取Class，可能抛出 ClassNotFoundException
CLass clazz = Class.forName("demo.Student");

//内值基本数据类型可以直接用类名.Type

//ClassLoader

```



### 哪些类型可以有Class对象

```
class 外部类，成员（成员内部类，静态内部类），局部内部类，匿名内部类
interface	接口
[]	数组
enum	枚举
annotation	注解@interface
primitive type	 基本数据类型
void
```



```java
	@Test
    public void ClassTest(){
        //类
        Class c1= Object.class;
        //接口
        Class c2=Comparable.class;
        //一维数组
        Class c3=String[].class;
        //二维数组
        Class c4=int[][].class;
        //注解
        Class c5=Override.class;
        //枚举
        Class c6=ElementType.class;
        //基本数据类型
        Class c7=Integer.TYPE;
        //void
        Class c8=void.class;
        //Class
        Class c9=Class.class;
    }
```



### java内存

堆

- 存放new的对象和数组
- 可以被所有的线程共享，不会存放别的对象引用

栈

- 存放基本变量类型（会包含这个基本类型的具体数值）
- 引用对象的变量（会存放这个引用在堆里面的具体地址）

方法区（特殊的堆）

- 可以被所有的线程共享
- 包含了所有的class和static变量、静态方法、静态变量、常量池、代码



### 类的加载过程

程序主动使用某个类时，如果该类还未被加载到内存中，则系统会通过三个步骤来对该类进行初始化

- 类的加载（Load）：将类的class文件读入内存，并为他创建一个java.lang.Class对象。此过程由类加载器完成

- 类的链接（Link）：将类的二进制数据合并到JRE中

- 类的初始化（Initialize）：JVM负责对类进行初始化



### 类的加载与ClassLoader

- 加载：将class文件字节码内容加载到内存中，并将这些静态数据转换城方法区的运行时数据结构，然后生成一个代表这个类的java.lang.Class对象

- 链接：将java类的二进制代码合并到JVM的运行状态之中的过程。
  - 验证：确保加载的类信息符合JVM规范，没有安全方面的问题
  - 准备：正式为类变量（static）分配内存并设置类变量默认初始值的阶段，这些内存都将在方法区中进行分配。
  - 解析：虚拟机常量池内的符号引用（常量名）替换为直接引用（地址）的过程
- 初始化：
  - 执行类构造器<clinit>()方法的过程。类构造器<clinit>()方法是由编译器自动收集类中所有类变量的赋值动作和静态代码块中的语句合并产生的。（类构造器是构造类信息的，不是构造该类对象的构造器）
  - 当初始化一个类的时候，如果发现其父类还没有进行初始化，则需要先触发其父类的初始化。
  - 虚拟机会保证一个类的<clinit>()方法在多线程环境中被正确加锁和同步





## p10

https://www.bilibili.com/video/BV1p4411P7V3?p=10













