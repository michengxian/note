# dubbo+zookeeper+springboot

> dubbo 的作用就是给消费端提供接口



搭建好zookeeper服务器集群

10.50.101.123:2181

10.50.101.123:2182

10.50.101.123:2183



## 流程：

1. 新建一个普通的pom项目
2. 在项目中新建一个普通的`pom` `module`，命名为dubbo-api
3. 在项目中新建一个`springboot` `module` ，命名dubbo-provider
4. 在项目中新建一个`springboot` `module` ，命名dubbo-consumer



## dubbo-api

在api中新建一个`interface`

```java
package dubboApi;

public interface TestServer {
    void sel();
}
```



## dubbo-provider

### pom.xml

```xml
	<dependencies>
        <!--dubbo-api-->
        <dependency>
            <groupId>org.example</groupId>
            <artifactId>dubbo-api</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>

        <dependency>
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo-spring-boot-starter</artifactId>
            <version>2.7.7</version>
            <exclusions>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-log4j12</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo</artifactId>
            <version>2.7.7</version>
        </dependency>
        <dependency>
            <groupId>org.apache.curator</groupId>
            <artifactId>curator-framework</artifactId>
            <version>4.1.0</version>
        </dependency>
        <dependency>
            <groupId>org.apache.curator</groupId>
            <artifactId>curator-recipes</artifactId>
            <version>4.1.0</version>
        </dependency>
        <dependency>
            <groupId>org.apache.zookeeper</groupId>
            <artifactId>zookeeper</artifactId>
            <version>3.4.13</version>
            <exclusions>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-log4j12</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.101tec</groupId>
            <artifactId>zkclient</artifactId>
            <version>0.10</version>
        </dependency>

      
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.junit.vintage</groupId>
                    <artifactId>junit-vintage-engine</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
    </dependencies>
```



### application.properties

```properties
spring.application.name=dubbo_auto_configuration_provider_demo

dubbo.application.name=dubbo_provider
dubbo.registry.protocol=zookeeper
dubbo.registry.address=zookeeper://10.50.101.123:2181,zookeeper://10.50.101.123:2182,zookeeper://10.50.101.123:2183
dubbo.protocol.name=dubbo
dubbo.protocol.port=20800

# impl包
dubbo.scan.base-packages=com.example.dubboprovider.server

server.port=8001
```



### TestServerImpl

```java
package com.example.dubboprovider.server;

import dubboApi.TestServer;
import org.apache.dubbo.config.annotation.DubboService;


@DubboService(version = "1.0.0",interfaceClass = TestServer.class)
public class TestServerImpl implements TestServer {
    @Override
    public void sel() {
        System.out.println("select");
    }
}
```



## dubbo-consumer

### pom.xml

同dubbo-provider



### application.properties

```properties
dubbo.application.name=dubbo_consumer
dubbo.registry.protocol=zookeeper
dubbo.registry.address=zookeeper://10.50.101.123:2181,zookeeper://10.50.101.123:2182,zookeeper://10.50.101.123:2183

#避免端口冲突
server.port=8002
```



### TestController

```java
package com.example.dubboconsumer.controller;

import dubboApi.TestServer;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @RequestMapping("/sel")
    public String sel(){
        testServiceImpl.sel();
        return "sel";
    }
}
```











