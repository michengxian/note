package com.mi.mallserver.mallenum;

public enum  MallExceptionEnum {


    NULL_EXCEPTION(10001,"空值异常"),
    REQUEST_EXCEPTION(10002,"请求异常"),
    REQUEST_DATA_EXCEPTION(10003,"请求参数异常"),
    DB_EXCEPTION(10004,"查询数据库异常"),
    SYS_EXCEPTION(10005,"系统异常"),
    OTHER_EXCEPTION(10100,"未知异常")
    ;



    private Integer code;

    private String desc;


    MallExceptionEnum(Integer code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public static String getDesc(MallExceptionEnum exceptionEnum){
        return exceptionEnum.desc;
    }

    public static Integer getCode(MallExceptionEnum exceptionEnum){
        return exceptionEnum.code;
    }


    public static String getDescByCode(Integer code){
        for (MallExceptionEnum exceptionEnum: MallExceptionEnum.values()){
            if (exceptionEnum.code == code){
                return exceptionEnum.desc;
            }
        }
        return null;
    }

    public static MallExceptionEnum getMallExceptionEnumByCode(Integer code){
        for (MallExceptionEnum exceptionEnum: MallExceptionEnum.values()){
            if (exceptionEnum.code == code){
                return exceptionEnum;
            }
        }
        return null;
    }


    public static MallExceptionEnum getMallExceptionEnumByMsg(String msg){
        for (MallExceptionEnum exceptionEnum: MallExceptionEnum.values()){
            if (exceptionEnum.desc.equals(msg)){
                return exceptionEnum;
            }
        }
        return OTHER_EXCEPTION;
    }
}
