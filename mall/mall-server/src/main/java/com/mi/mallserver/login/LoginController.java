package com.mi.mallserver.login;

import com.mi.mallserver.base.BaseRequest;
import com.mi.mallserver.base.BaseResponse;
import com.mi.mallserver.bean.LoginReqBean;
import com.mi.mallserver.bean.LoginResBean;
import com.mi.mallserver.exception.MallException;
import com.mi.mallserver.mallenum.MallExceptionEnum;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

    @PostMapping("/login")
    public BaseResponse<LoginResBean> login(@RequestBody BaseRequest<LoginReqBean> reqBean){
        System.out.println("seqNo:"+reqBean.getSeqNo()+" sys:"+reqBean.getSys()+" ver:"+reqBean.getVer());
        if (reqBean.getRequest()==null){
            throw new MallException(MallExceptionEnum.SYS_EXCEPTION);
        }
        System.out.println("name:"+reqBean.getRequest().getName() + " pwd:"+reqBean.getRequest().getPwd());

        LoginResBean resBean = new LoginResBean();
        resBean.setName(reqBean.getRequest().getName());
        resBean.setAge("20");
        resBean.setSex("男");

        BaseResponse<LoginResBean> response = new BaseResponse<>();
        response.setSeqNo(reqBean.getSeqNo());
        response.setCode(1000);
        response.setDesc("成功");
        response.setData(resBean);

        return response;
    }





    @PostMapping("test")
    public String test(){



    }

}
