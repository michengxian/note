package com.mi.mallserver.base;

import lombok.Data;

@Data
public class BaseRequest<T> {

    //时间戳
    private String seqNo;

    //系统
    private String sys;

    //版本号
    private String ver;

    //真正的请求参数
    private T request;

}
