package com.mi.mallserver.resolver;

import com.mi.mallserver.base.BaseResponse;
import com.mi.mallserver.mallenum.MallExceptionEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@RestControllerAdvice
@Slf4j
public class MallExceptionResolver {


    @ExceptionHandler(value = Exception.class)
    public BaseResponse handler(Exception ex) {
        log.error("统一异常处理", ex);
        BaseResponse response = new BaseResponse();
        MallExceptionEnum exceptionEnum = MallExceptionEnum.getMallExceptionEnumByMsg(ex.getMessage());
        response.setCode(MallExceptionEnum.getCode(exceptionEnum));
        response.setDesc(MallExceptionEnum.getDesc(exceptionEnum));
        return response;
    }

}
