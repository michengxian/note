package com.mi.mallserver.aop;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

@Aspect
@Component
@Slf4j
public class MallControllerAop {

    @Pointcut("execution(* com.mi.mallserver.*.*Controller.*(..))")
    public void pointCut(){

    }


    @Around("pointCut()")
    public Object around(ProceedingJoinPoint pjp){
        Object o = null;
        try {
            String req = "";
            RequestMapping reqs = pjp.getTarget().getClass().getAnnotation(RequestMapping.class);
            Annotation[] annotations = null;
            Method[] methods = pjp.getTarget().getClass().getMethods();
            Signature signature = pjp.getSignature();
            for (Method method :pjp.getTarget().getClass().getMethods()){
                if (method.getName().equals(pjp.getSignature().getName())){
                    annotations = method.getAnnotations();
                    break;
                }
            }
            if (reqs!=null){
                req = reqs.value()[0];
            }
            for (Annotation a : annotations){
                if (a instanceof RequestMapping){
                    req = req+ ((RequestMapping) a).value()[0];
                }
                else if (a instanceof PostMapping){
                    req = req+ ((PostMapping) a).value()[0];
                }
                else if (a instanceof GetMapping){
                    req = req+ ((GetMapping) a).value()[0];
                }
            }

            log.info("start request class:{} url:{} params:{}",pjp.getTarget().getClass().getSimpleName(),req, JSON.toJSONString(pjp.getArgs()));
            o = pjp.proceed(pjp.getArgs());
            log.info("end  request class:{} url:{} res:{}",pjp.getTarget().getClass().getSimpleName(),req,JSON.toJSONString(o));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return o;
    }

}
