package com.mi.mallserver.exception;

import com.mi.mallserver.mallenum.MallExceptionEnum;

public class MallException extends RuntimeException {

    public MallException(String msg){
        super(msg);
    }

    public MallException(Integer code){
        super(MallExceptionEnum.getDescByCode(code));
    }

    public MallException(MallExceptionEnum exceptionEnum){
        super(MallExceptionEnum.getDesc(exceptionEnum));
    }

}
