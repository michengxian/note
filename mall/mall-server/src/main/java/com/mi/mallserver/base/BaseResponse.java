package com.mi.mallserver.base;

import lombok.Data;

@Data
public class BaseResponse<T> {

    //时间戳
    private String seqNo;

    //返回参数
    private Integer code;

    //返回的描述符
    private String desc;

    //真正的请求参数
    private T data;

}
