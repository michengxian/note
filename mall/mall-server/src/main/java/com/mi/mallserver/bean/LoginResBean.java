package com.mi.mallserver.bean;

import lombok.Data;

@Data
public class LoginResBean {

    private String name;

    private String age;

    private String sex;
}
