package com.mi.mallserver.bean;

import lombok.Data;

@Data
public class LoginReqBean {

    private String name;

    private String pwd;
}
