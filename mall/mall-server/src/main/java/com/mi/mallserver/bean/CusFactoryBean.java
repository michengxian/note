package com.mi.mallserver.bean;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

@Component
public class CusFactoryBean implements FactoryBean {
    @Override
    public Object getObject() throws Exception {
        return new CusFactoryBean();
    }

    @Override
    public Class<?> getObjectType() {
        return CusFactoryBean.class;
    }
}
