# kafka

## 环境

```bash
# 安装zookeeper
docker run -d --name zookeeper -p 2181:2181 -t wurstmeister/zookeeper
# 安装kafka
docker run -d --name kafka -p 9092:9092 -e KAFKA_BROKER_ID=0 -e KAFKA_ZOOKEEPER_CONNECT=192.168.1.5:2181 -e KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://192.168.1.5:9092 -e KAFKA_LISTENERS=PLAINTEXT://0.0.0.0:9092 wurstmeister/kafka
# 进入kafka容器
docker exec -it kafka bash
# 进入目录
cd /opt/kafka/bin
# 创建topic
./kafka-topics.sh --create --topic user --replication-factor 1 --partitions 1 --zookeeper localhost:2181
# 查看所有topic
./kafka-topics.sh --zookeeper 192.168.1.5 --list
# 创建生产者
./kafka-console-producer.sh --broker-list localhost:9092 --topic sun
# 创建消费者
./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic sun --from-beginning
```

## 注意


### 注意生产者的key一致

```java
	@GetMapping("/user")
    public void sendUser() {
        User user = getUser();
        kafkaTemplate.send("user",
                "user：book",
                JSON.toJSONString(user));
        log.info("User 生产者生产数据：User:{}",JSON.toJSONString(user));
    }



    @GetMapping("/book")
    public void sendBook() {
        Book book = getBook();
        kafkaTemplate.send("book",
                "user：book",
                JSON.toJSONString(book));
        log.info("Book 生产者生产数据：Book:{}",JSON.toJSONString(book));
    }
```

两个生产者send的key不一致的时候，导致hash到不同的分区

```java

KStream<String,User> userKStream = builder.stream("user", Consumed.with(KafkaSerdes.String(),KafkaSerdes.UserSerde()))
                .filter((k,v)->null!=k&&null!=v)
                .mapValues((k,v)->{
                    log.info("userKStream K:{} v:{}",k,v);
                    return v;
                });


        KTable<String, Book> bookTable= builder.table("book", Consumed.with(KafkaSerdes.String(),KafkaSerdes.BookSerde()))
                .filter((k,v)->null!=k&&null!=v)
                .mapValues((k,v)->{
                    log.info("userTable K:{} v:{}",k,v);
                    return v;
                });

        // 使用Lambda表达式创建连接器
        ValueJoiner<User, Book, Sun> valueJoiner = (user, book) -> {

            Sun sun = new Sun();
            if (null!=user) {
                sun.setUserName(user.getName());
                sun.setUserAge(user.getAge());
            }
            if (null!=book) {
                sun.setBookName(book.getName());
                sun.setBookPrice(book.getPrice());
            }
            return sun;
        };

        KStream<String, Sun> sunKStream = userKStream.leftJoin(bookTable,
                valueJoiner,
                Joined.with(KafkaSerdes.String(),
                        KafkaSerdes.UserSerde(),
                        KafkaSerdes.BookSerde()));
        sunKStream.print(Printed.<String, Sun>toSysOut().withLabel("Transactions and News"));

```

leftJoin时 valueJoiner 里面book一直为空，因为book和user不在同一个分区内
join时 不会进 valueJoiner


### 注意List<T>对象

#### 方式一：


```java

	@Override
    public List<T> deserialize(String topic, byte[] data) {
        if (data == null) {
            return null;
        }
        List<T> list  = gson.fromJson(new String(data), deserializedClass);
        List<T> tList = new ArrayList<>(list.size());
        list.forEach(item -> tList.add(gson.fromJson(gson.toJson(item), cls)));
        return tList;
    }

```

直接gson.fromJson只能获取到List<LinkedTreeList>对象，无法获取我们想要的List<T>对象

#### 方式二：

用对象引用

```java


@Data
@ToString
public class BookList {

    private List<Book> bookList;

}

```

#### 方式三：

```java
public class JsonListSerializer<T> implements Serializer<List> {

    com.google.gson.JsonSerializer<Date> ser = (src, typeOfSrc, context)-> src == null ? null : new JsonPrimitive(src.getTime());

//    private Gson gson = new Gson();
    ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public byte[] serialize(String s, List list) {
//        return gson.toJson(list).getBytes(Charset.forName("UTF-8"));
        byte[] bytes = null;
        try {
            bytes = objectMapper.writeValueAsBytes(list);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return bytes;
    }
}

```


```java

public class JsonListDeserializer<T> implements Deserializer<List> {


//    private Gson gson = new Gson();

    ObjectMapper objectMapper = new ObjectMapper();


    private Class<T> cls;


    public JsonListDeserializer(Class<T> cls) {
        this.cls = cls;
    }


    @Override
    public List<T> deserialize(String topic, byte[] data) {
        if (data == null) {
            return null;
        }

        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(List.class, cls);
        List<T> list = null;
        try {
            list = objectMapper.readValue(data, javaType);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }


}

```

