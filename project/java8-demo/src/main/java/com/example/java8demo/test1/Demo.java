package com.example.java8demo.test1;

import com.example.java8demo.bean.UserBean;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Objects;
import java.util.function.*;

@Slf4j
public class Demo {

    public static void main(String[] args) {
        List<UserBean> userBeans = UserBean.defaultUserBeanList();

//        Function<Integer,Integer> f = x->x+1;
//        Function<Integer,Integer> g = x->x*2;
//        Function<Integer,Integer> h = f.andThen(g);
//
//        int res = h.apply(1);
//        log.info("res:{}",res);
//
//
//        Function<Integer,Integer> r = ((Function<Integer,Integer>)x->x+1).andThen(x->x*2);
//
//        Predicate<UserBean> notNullPredicate = Objects::nonNull;
//        Predicate<UserBean> predicate = notNullPredicate.and(v->v.getAge()>2).or(v->v.getName().contains("1"));
//        userBeans.forEach(v->{
//            boolean flag = predicate.test(v);
//            log.info("v:{} flag :{}", v ,flag);
//        });


//        Consumer<UserBean> c = userBean -> {
//            log.info("run some things ：{}",userBean);
//        };
//        userBeans.forEach(v->{
//            log.info("start send userBeans to consumer :{}",v);
//            c.accept(v);
//            log.info("end send userBeans to consumer :{}",v);
//        });


//        Supplier<UserBean> supplier = ()->{
//            log.info("start new bean");
//            UserBean bean = new UserBean("mi",19);
//            log.info("end new bean");
//            return bean;
//        };
//
//        log.info("userBean : {}",supplier.get());


//        UnaryOperator<UserBean> unaryOperator = userBean -> {
//            log.info("do ....");
//            userBean.setAge(userBean.getAge()+1);
//            return userBean;
//        };
//
//        userBeans.forEach(v->{
//            log.info("start ...");
//            UserBean bean  = unaryOperator.apply(v);
//            log.info("end... {}",bean);
//        });
//
//
//        UnaryOperator<Integer> uo = a->{
//            log.info("a:{}",a);
//            return a++;
//        };
//        log.info("start operator");
//        uo.apply(1);
//        log.info("end operator");
//
//        BinaryOperator<Integer> res = (a,b)->{
//            log.info("a:{} b:{}",a,b);
//            return a+b;
//        };
//        log.info("start operator");
//        res.apply(1,2);
//        log.info("end operator");


//        BiPredicate<Integer,UserBean> re = (integer, userBean) -> {
//            log.info("integer ：{} , userBean：{}",integer,userBean);
//            return userBean.getAge()==integer;
//        };
//        userBeans.forEach(v->{
//            log.info("v:{}",v);
//            boolean b= re.test(1,v);
//            log.info("test:{}",b);
//        });

//
//        BiConsumer<Integer,UserBean> bc = (integer, userBean) -> {
//            log.info("integer ：{} , userBean：{}",integer,userBean);
//            userBean.setAge(integer);
//        } ;
//        userBeans.forEach(v->{
//            log.info("start v:{}",v);
//            bc.accept(19,v);
//            log.info("end v:{}",v);
//        });

        BiFunction<String,Integer,UserBean> bf=(s, integer) -> {
            log.info("s ：{} , integer：{}",s,integer);
            return new UserBean(s,integer);
        };
        UserBean userBean = bf.apply("mi",18);
        log.info("userBean:{}",userBean);
    }
}
