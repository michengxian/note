package com.example.java8demo.test;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class Demo {

    public static void main(String[] args) {

        Trader raou1=new Trader("Raou1","Cambridge");
        Trader mario=new Trader("Mario","Milan");
        Trader alan=new Trader("Alan","Cambridge");
        Trader brian=new Trader("Brian","Cambridge");

        List<Transaction> transactions = Arrays.asList(
                new Transaction(brian,2011,300),
                new Transaction(raou1,2012,1000),
                new Transaction(raou1,2011,400),
                new Transaction(mario,2012,710),
                new Transaction(mario,2012,700),
                new Transaction(alan,2012,950)
        );


        transactions.stream()
                .filter(v->v.getYear()==2011)
                .sorted(Comparator.comparingInt(Transaction::getValue))
                .collect(Collectors.toList())
                .forEach(v->log.info("value:{}",v));

        transactions.stream().map(Transaction::getTrader).map(Trader::getCity).distinct().collect(Collectors.toList()).forEach(v->log.info("city:{}",v));

        transactions.stream()
                .map(Transaction::getTrader)
                .filter(v->v.getCity().equals("Cambridge"))
                .map(Trader::getName)
                .sorted()
                .collect(Collectors.toList())
                .forEach(v->log.info("name:{}",v));

        transactions.stream().map(Transaction::getTrader)
                .map(v->v.getName().split(""))
                .flatMap(Arrays::stream)
                .collect(Collectors.toList())
                .forEach(v->log.info("name char :{}",v));

        boolean flag = transactions.stream().anyMatch(v->v.getTrader().getCity().equals("Milan"));
        log.info("flag :{}",flag);

        int sum = transactions.stream()
                .filter(v->v.getTrader()
                        .getCity().equals("Cambridge"))
                .map(Transaction::getValue)
                .reduce(0,(o1,o2)->o1+o2);
        log.info("sum:{}",sum);

        int max = transactions.stream().max(Comparator.comparingInt(Transaction::getValue)).get().getValue();
        log.info("max:{}", max);


        Transaction minTransaction = transactions.stream().min(Comparator.comparing(Transaction::getValue)).get();
        log.info("minTransaction:{}",minTransaction);

    }

}
