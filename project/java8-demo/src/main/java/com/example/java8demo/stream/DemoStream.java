package com.example.java8demo.stream;

import com.example.java8demo.bean.UserBean;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class DemoStream {

    public static void main(String[] args) {

        List<UserBean> list = UserBean.defaultUserBeanList();
        list.forEach(v->log.info("v ：{}",v));

        List<UserBean> filterList = list.stream().filter(v->v.getAge()>0).collect(Collectors.toList());
        filterList.forEach(v->log.info("filter:{}",v));


        boolean flag1 = list.stream().allMatch(v->v.getAge()==3);
        log.info("flag1 :{}",flag1);

        boolean flag2 = list.stream().anyMatch(v->v.getAge()==3);
        log.info("flag2 :{}",flag2);

        boolean flag3 = list.stream().noneMatch(v->v.getAge()==3);
        log.info("flag3 :{}",flag3);

        Optional<UserBean> bean1 = list.stream().findFirst();
        log.info("bean1 :{}",bean1);

        Optional<UserBean> bean2 = list.stream().findAny();
        log.info("bean2 :{}",bean2);

        Optional<UserBean> bean3 = list.stream().findAny();
        log.info("bean3 :{}",bean3);


//        list.stream().limit(1).map(v->v.getName().split("")).flatMap(Arrays::stream).collect(Collectors.toList()).forEach(v->log.info("flatMap:{}",v));


        List<String> stringList = Arrays.asList("a","b","c","a","d");
//        stringList.stream().filter(v->v.equals("a")).collect(Collectors.toList()).forEach(v->log.info("filter:{}",v));
//        stringList.stream().distinct().collect(Collectors.toList()).forEach(v->log.info("distinct:{}",v));
//        stringList.stream().limit(2).collect(Collectors.toList()).forEach(v->log.info("limit:{}",v));
//        stringList.stream().skip(2).collect(Collectors.toList()).forEach(v->log.info("skip:{}",v));

        String s= stringList.stream().reduce("0",(o1,o2)->o1+o2);
        log.info(s);

        Optional<String> reduce = stringList.stream().reduce((o1, o2) -> o1 + o2);
        log.info(reduce.toString());

    }





}
