package com.example.java8demo.bean;

import java.util.ArrayList;
import java.util.List;

public class UserBean {

    private String name;

    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public UserBean(){}

    public UserBean(String name,Integer age){
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    private static UserBean defaultUserBean(int i){
        UserBean bean = new UserBean();
        bean.setName("name " + i);
        bean.setAge(i);
        return bean;
    }


    public static List<UserBean> defaultUserBeanList(){
        List<UserBean> list = new ArrayList<>();
        for (int i=0;i<4;i++){
            list.add(defaultUserBean(i));
        }
//        list.add(defaultUserBean(3));
//        list.add(defaultUserBean(4));
//        list.add(defaultUserBean(5));
        return list;
    }

}
