package com.example.kafka.config;

import com.example.kafka.bean.Book;
import com.example.kafka.bean.Message;
import com.example.kafka.bean.Sun;
import com.example.kafka.bean.User;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.stereotype.Component;

import java.util.*;

@Configuration
@Slf4j
@EnableKafkaStreams
public class CusKafkaTable {

    @Autowired
    private Properties properties;


    @Bean
    public KStream<String, List<Sun>> kStream(StreamsBuilder builder) {

        KStream<String,List> userkStream= builder.stream("userList",Consumed.with(KafkaSerdes.String(),KafkaSerdes.UserListSerde()));
        KStream<String,List> bookkStream= builder.stream("bookList",Consumed.with(KafkaSerdes.String(),KafkaSerdes.BookListSerde()));





        KTable<String,List> userkTable = builder.table("user",Consumed.with(KafkaSerdes.String(),KafkaSerdes.UserListSerde()));
//        KTable<String,List> bookkTable = builder.table("book",Consumed.with(KafkaSerdes.String(),KafkaSerdes.BookListSerde()));

        KStream<String, List<Sun>> kStream = bookkStream.join(userkTable, this::bookLeftJoinUser)
                .mapValues((k,v)-> {
                    log.info("kStream-> k:{} , v:{}", k, v);
                    return v;
                });

//        KTable<String, List<Sun>>  kTable = bookkTable.join(userkTable, this::bookLeftJoinUser)
//                .mapValues((k,v)-> {
//                    log.info("kTable-> k:{} , v:{}", k, v);
//                    return v;
//                });;

        return kStream;

//        KTable<String, List> userKTable = builder.table("user", Consumed.with(KafkaSerdes.String(),KafkaSerdes.UserListSerde())
//                .withTimestampExtractor(new TransactionTimestampExtractor()))
//                .filter((k,v)->null!=k&&null!=v);
//
//
//        KTable<String, List> bookTable= builder.table("book", Consumed.with(KafkaSerdes.String(),KafkaSerdes.BookListSerde())
//                .withTimestampExtractor(new TransactionTimestampExtractor()))
//                .filter((k,v)->null!=k&&null!=v);
//
//        // 使用Lambda表达式创建连接器
//
//        KStream<String, List<Sun>> sunKStream = bookTable.join(userKTable,
//                (bookList ,userList) -> bookLeftJoinUser(bookList,userList)).toStream();
//        sunKStream.print(Printed.<String, List<Sun>>toSysOut().withLabel("Transactions and News"));
//
//
//
//        sunKStream.to("sun");

//        return sunKStream;

    }

    private List<Sun> bookLeftJoinUser(List bookList,List userList,List<Sun> sunList){

        sunList = bookLeftJoinUser(bookList,userList);

        return sunList;
    }

    private List<Sun> bookLeftJoinUser(List bookList,List userList){
        List<Sun> list = new ArrayList<>();

        bookList.stream()
                .forEach(item->userList.stream()
                        .filter(v->Objects.equals(((User)v).getId(),((Book)item).getId()))
                        .forEach(v->{
                            User user =  (User)v;
                            Book book = (Book) item;
                            Sun sun = new Sun();
                            sun.setId(book.getId());
                            sun.setBookName(book.getName());
                            sun.setBookPrice(book.getPrice());
                            if (null!=user) {
                                sun.setUserName(user.getName());
                                sun.setUserAge(user.getAge());
                                list.add(sun);
                            }
                        }));
        return list;
    }


    /* v1
    KStream<String,Book> bookKStream = builder.stream("book", Consumed.with(KafkaSerdes.String(),KafkaSerdes.BookSerde()))
                .filter((k,v)->null!=k&&null!=v)
                .mapValues((k,v)->{
                    log.info("userTable K:{} v:{}",k,v);
                    return v;
                });

        KTable<String,User> userTable = builder.table("user", Consumed.with(KafkaSerdes.String(),KafkaSerdes.UserSerde()))
                .filter((k,v)->null!=k&&null!=v)
                .mapValues((k,v)->{
                    log.info("userTable K:{} v:{}",k,v);
                    return v;
                });

                //不会进这里
    KStream<String,Sun> sunKStream = userKStream.join(bookTable, (user, book) -> {
            Sun sun = new Sun();
            sun.setUserName(user.getName());
            sun.setUserAge(user.getAge());
            sun.setBookName(book.getName());
            sun.setBookPrice(book.getPrice());
            return sun;
        });

     */


    /* V2
     KStream<String,User> userKStream = builder.stream("user", Consumed.with(KafkaSerdes.String(),KafkaSerdes.UserSerde()))
                .filter((k,v)->null!=k&&null!=v)
                .mapValues((k,v)->{
                    log.info("userKStream K:{} v:{}",k,v);
                    return v;
                });


        KTable<String, Book> bookTable= builder.table("book", Consumed.with(KafkaSerdes.String(),KafkaSerdes.BookSerde()))
                .filter((k,v)->null!=k&&null!=v)
                .mapValues((k,v)->{
                    log.info("userTable K:{} v:{}",k,v);
                    return v;
                });

        // 使用Lambda表达式创建连接器
        ValueJoiner<User, Book, Sun> valueJoiner = (user, book) -> {

            Sun sun = new Sun();
            if (null!=user) {
                sun.setUserName(user.getName());
                sun.setUserAge(user.getAge());
            }
            if (null!=book) {
                sun.setBookName(book.getName());
                sun.setBookPrice(book.getPrice());
            }
            return sun;
        };

        KStream<String, Sun> sunKStream = userKStream.leftJoin(bookTable,
                valueJoiner,
                Joined.with(KafkaSerdes.String(),
                        KafkaSerdes.UserSerde(),
                        KafkaSerdes.BookSerde()));
        sunKStream.print(Printed.<String, Sun>toSysOut().withLabel("Transactions and News"));


     */



    /* V3 success

        KTable<String,User> userKTable = builder.table("user", Consumed.with(KafkaSerdes.String(),KafkaSerdes.UserSerde())
                .withTimestampExtractor(new TransactionTimestampExtractor()))
                .filter((k,v)->null!=k&&null!=v)
                .mapValues((k,v)->{
                    log.info("userKStream K:{} v:{}",k,v);
                    return v;
                });


        KTable<String, Book> bookTable= builder.table("book", Consumed.with(KafkaSerdes.String(),KafkaSerdes.BookSerde())
                .withTimestampExtractor(new TransactionTimestampExtractor()))
                .filter((k,v)->null!=k&&null!=v)
                .mapValues((k,v)->{
                    log.info("userTable K:{} v:{}",k,v);
                    return v;
                });

        // 使用Lambda表达式创建连接器

        KStream<String, Sun> sunKStream = bookTable.join(userKTable,
                (book ,user) -> {
                    Sun sun = new Sun();
                    if (null!=user) {
                        sun.setUserName(user.getName());
                        sun.setUserAge(user.getAge());
                    }
                    if (null!=book) {
                        sun.setBookName(book.getName());
                        sun.setBookPrice(book.getPrice());
                    }
                    return sun;
                }).toStream();
        sunKStream.print(Printed.<String, Sun>toSysOut().withLabel("Transactions and News"));



        sunKStream.to("sun");

        return sunKStream;

     */


}
