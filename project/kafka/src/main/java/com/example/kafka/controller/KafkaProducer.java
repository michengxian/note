package com.example.kafka.controller;


import com.alibaba.fastjson.JSON;
import com.example.kafka.bean.Book;
import com.example.kafka.bean.Message;
import com.example.kafka.bean.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

@RestController
@Slf4j
public class KafkaProducer {
    @Resource
    private KafkaTemplate<String, Object> kafkaTemplate;



    @GetMapping("/userList")
    public void sendUserList() {
        List<User> list = new ArrayList<>();
        list.add(User.getUser());
        list.add(User.getUser());
        kafkaTemplate.send("user",
                "userList：bookList",
                JSON.toJSONString(list));

        kafkaTemplate.send("userList",
                "userList：bookList",
                JSON.toJSONString(list));

        log.info("sendUserList 生产者生产数据：UserList:{}",JSON.toJSONString(list));
    }



    @GetMapping("/bookList")
    public void sendBookList() {
        List<Book> list = new ArrayList<>();
        list.add(Book.getBook());
        list.add(Book.getBook());
        kafkaTemplate.send("book",
                "userList：bookList",
                JSON.toJSONString(list));

        kafkaTemplate.send("bookList",
                "userList：bookList",
                JSON.toJSONString(list));

        log.info("sendBookList 生产者生产数据：BookList:{}",JSON.toJSONString(list));
    }


    @GetMapping("/user")
    public void sendUser() {
        User user = User.getUser();
        kafkaTemplate.send("user",
                "user：book",
                JSON.toJSONString(user));
        log.info("User 生产者生产数据：User:{}",JSON.toJSONString(user));
    }



    @GetMapping("/book")
    public void sendBook() {
        Book book = Book.getBook();
        kafkaTemplate.send("book",
                "user：book",
                JSON.toJSONString(book));
        log.info("Book 生产者生产数据：Book:{}",JSON.toJSONString(book));
    }
}