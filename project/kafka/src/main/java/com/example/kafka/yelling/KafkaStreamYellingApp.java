package com.example.kafka.yelling;

import com.example.kafka.config.KafkaSerdes;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;

import java.util.Properties;

@Slf4j
public class KafkaStreamYellingApp {

    public static void main(String[] args) throws InterruptedException {

        Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG,"yelling_app_id");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,"192.168.1.5:9092");

        StreamsBuilder builder = new StreamsBuilder();

        KStream<String,String> stream = builder.stream("src-topic", Consumed.with(KafkaSerdes.String(),KafkaSerdes.String())).mapValues((k,v)->{
            log.info("stream k:{} v:{}",k,v);
            return v;
        });

        KStream<String,String> outStream = builder.stream("out-topic", Consumed.with(KafkaSerdes.String(),KafkaSerdes.String())).mapValues((k,v)->{
            log.info("outStream k:{} v:{}",k,v);
            return v;
        });


        KStream<String,String> upperCaseStream = stream.mapValues((k,v)->v.toUpperCase());

        upperCaseStream.to("out-topic", Produced.with(KafkaSerdes.String(),KafkaSerdes.String()));

        KafkaStreams kafkaStreams = new KafkaStreams(builder.build(),properties);
        kafkaStreams.start();
        Thread.sleep(35000);
        log.info("shutting down the yelling app now");
        kafkaStreams.close();
    }

}
