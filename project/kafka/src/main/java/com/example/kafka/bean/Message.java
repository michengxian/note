package com.example.kafka.bean;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Message {

    private User message;
}
