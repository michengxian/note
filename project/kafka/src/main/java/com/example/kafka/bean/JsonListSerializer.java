package com.example.kafka.bean;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonPrimitive;
import com.google.gson.reflect.TypeToken;
import org.apache.kafka.common.serialization.Serializer;

import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;

public class JsonListSerializer<T> implements Serializer<List> {

    com.google.gson.JsonSerializer<Date> ser = (src, typeOfSrc, context)-> src == null ? null : new JsonPrimitive(src.getTime());

//    private Gson gson = new Gson();
    ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public byte[] serialize(String s, List list) {
//        return gson.toJson(list).getBytes(Charset.forName("UTF-8"));
        byte[] bytes = null;
        try {
            bytes = objectMapper.writeValueAsBytes(list);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return bytes;
    }
}
