package com.example.kafka.config;

import com.example.kafka.bean.Book;
import com.example.kafka.bean.Sun;
import com.example.kafka.bean.User;
import org.apache.kafka.streams.kstream.ValueJoiner;

public class SunJoiner implements ValueJoiner<User, Book, Sun> {


    @Override
    public Sun apply(User user, Book book) {

        Sun sun = new Sun();
        if (null!=user) {
            sun.setUserName(user.getName());
            sun.setUserAge(user.getAge());
        }
        if (null!=book) {
            sun.setBookName(book.getName());
            sun.setBookPrice(book.getPrice());
        }
        return sun;
    }
}
