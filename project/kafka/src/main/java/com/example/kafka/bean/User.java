package com.example.kafka.bean;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class User {

    private Integer id;

    private String name;

    private int age;

    public static User getUser(){
        User user = new User();
        user.setId((int) (Math.random()*10 / 1));
        user.setName("用户"+ Math.random()*100);
        user.setAge((int) (Math.random()*100 / 1));
        return user;
    }

}
