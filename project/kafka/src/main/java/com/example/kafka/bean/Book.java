package com.example.kafka.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
//import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ToString
//@Builder
public class Book {

//    @JsonProperty("b_id")
    private Integer id;

//    @JsonProperty("b_name")
    private String name;

//    @JsonProperty("b_price")
    private Double price;

//    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date productTime;


    public Book(){}


    public static Book getBook(){
        Book book = new Book();
        book.setId((int) (Math.random()*10 / 1));
        book.setName("书："+ Math.random()*100);
        book.setPrice(Math.random()*200);
        book.setProductTime(new Date());
        return book;
    }


}
