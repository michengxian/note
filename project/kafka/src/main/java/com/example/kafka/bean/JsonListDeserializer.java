package com.example.kafka.bean;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import lombok.val;
import org.apache.kafka.common.serialization.Deserializer;
import sun.rmi.runtime.Log;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class JsonListDeserializer<T> implements Deserializer<List> {


//    private Gson gson = new Gson();

    ObjectMapper objectMapper = new ObjectMapper();


    private Class<T> cls;


    public JsonListDeserializer(Class<T> cls) {
        this.cls = cls;
    }


    @Override
    public List<T> deserialize(String topic, byte[] data) {
        if (data == null) {
            return null;
        }

        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(List.class, cls);
        List<T> list = null;
        try {
            list = objectMapper.readValue(data, javaType);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }


}
