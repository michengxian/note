package com.example.kafka.bean;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class Sun {

    private Integer id;

    private String bookName;

    private Double bookPrice;

    private String userName;

    private int userAge;

}
