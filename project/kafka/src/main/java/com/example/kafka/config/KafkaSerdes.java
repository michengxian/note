package com.example.kafka.config;

import com.example.kafka.bean.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;

import java.util.List;
import java.util.Map;

@Slf4j
public class KafkaSerdes extends Serdes {


    public static Serde<List> UserListSerde(){
        return new UserListSerde();
    }


    public static final class UserListSerde extends WrapperSerde<List> {
        public UserListSerde() {
            super(new JsonListSerializer<>(),
                    new JsonListDeserializer<>(User.class));
        }
    }


    public static Serde<List> BookListSerde(){
        return new BookListSerde();
    }

    public static final class BookListSerde extends WrapperSerde<List> {
        public BookListSerde() {
            super(new JsonListSerializer<>(),
                    new JsonListDeserializer<>(Book.class));
        }
    }


    public static Serde<User> UserSerde(){
        return new UserSerde();
    }


    public static final class UserSerde extends WrapperSerde<User> {
        public UserSerde() {
            super(new JsonSerializer<>(),
                    new JsonDeserializer<>(User.class));
        }
    }



    public static Serde<Book> BookSerde(){
        return new BookSerde();
    }

    public static final class BookSerde extends WrapperSerde<Book> {
        public BookSerde() {
            super(new JsonSerializer<>(),
                    new JsonDeserializer<>(Book.class));
        }
    }


    public static Serde<Sun> SunSerde(){
        return new SunSerde();
    }

    public static final class SunSerde extends WrapperSerde<Sun> {
        public SunSerde() {
            super(new JsonSerializer<>(),
                    new JsonDeserializer<>(Sun.class));
        }
    }


}
